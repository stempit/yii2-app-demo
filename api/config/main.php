<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'blabla-api',
    'name' => 'API ' . $params['brandName'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\controllers',
    'components' => [
        'user' => [
            'enableSession' => false,
            'identityClass' => 'common\model\User',
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'log' => [
            'targets' => [
                'notFound' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/404.log',
                    'rotateByCopy' => strtoupper(substr(PHP_OS, 0, 3)) === 'WIN',
                    'levels' => ['error'],
                    'categories' => ['yii\web\HttpException:404'],
                    'logVars' => [],
                ],
                'errorCommon' => [
                    'except' => ['yii\web\HttpException:404'],
                ],
            ],
        ],
        'mailer' => [
            'messageConfig' => [
                'from' => [$params['emailInfo'] => $params['brandName']],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => ''],
            ],
        ],
        'frontendUrlManager' => [
            'class' => 'frontend\components\FrontendUrlManager',
        ],
    ],
    'params' => $params,
];
