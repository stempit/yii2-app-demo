<?php
return [
    'components' => [
        'cacheCommon' => [
            'class' => '',
        ],
        'cache' => [
            'class' => '',
            // if you do not use files, specify 'cache' in each main-local
        ],
        'db' => [
            'dsn' => 'pgsql:dbname=blabla',
            'username' => 'blabla',
            'password' => '',
        ],
    ],
];
