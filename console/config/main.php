<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'blabla-console',
    'name' => 'console app of ' . $params['brandName'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'db' => [
            'schemaCache' => 'cache',
        ],
        'log' => [
            'targets' => [
                'sql' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/sql.log',
                    'rotateByCopy' => (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'),
                    'levels' => ['info'],
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                ],
                'emailError' => [
                    'class' => 'common\log\EmailTarget',
                    'schemaCache' => 'cache',
                    'levels' => ['error'],
                    'logVars' => [],
                    'message' => [
                        'to' => [$params['emailBugs']],
                        'subject' => 'Console error',
                    ],
                ],
            ],
        ],
        'mailer' => [
            'messageConfig' => [
                'from' => [$params['emailInfo'] => $params['brandName']],
            ],
        ],
        'frontendUrlManager' => [
            'class' => 'frontend\components\FrontendUrlManager',
        ],
    ],
    'params' => $params,
];
