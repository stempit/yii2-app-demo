<?php

use common\models\User;
use yii\db\Migration;

class m151223_092603_user_insert extends Migration
{
    /**
     * @var string
     */
    protected $userEmail = 'stempit@gmail.com';

    /**
     * @inheritdoc
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $this->insert(
            User::tableName(),
            [
                'role'   => 'admin',
                'title'  => 'Web Developer',
                'email'  => $this->userEmail,

                'password_hash' => Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->getSecurity()->generateRandomString()),
            ]
        );

        User::sendEmailInvite($this->userEmail);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->delete(
            User::tableName(),
            ['email' => $this->userEmail]
        );
    }
}
