<?php

use yii\db\Migration;
use console\controllers\DbController;

class m151223_062424_create_pgcrypto extends Migration
{
	/**
	 * @inheritdoc
	 * @throws \yii\console\Exception
	 */
	public function up()
	{
		DbController::executeSqlConsole('CREATE EXTENSION pgcrypto;');
	}

	/**
	 * @inheritdoc
	 * @throws \yii\console\Exception
	 */
	public function down()
	{
		DbController::executeSqlConsole('DROP EXTENSION pgcrypto;');
	}
}
