<?php

use \yii\db\Migration;

class m160608_094814_created_trigger_function extends Migration
{
    /**
     * @var string
     */
    protected $functionName = 'set_created';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function up()
    {
        echo '    > create function ', $this->functionName, '() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->functionName}()
RETURNS TRIGGER AS
$$
BEGIN
  NEW.created := NOW();
  RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function down()
    {
        echo '    > drop function ', $this->functionName, '() ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION ' . $this->functionName . '();')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
