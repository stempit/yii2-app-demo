<?php

use yii\db\Migration;
use console\controllers\DbController;

class m151223_071538_user extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'user';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        try {
            // warning is in stderr if pgcrypto exist
            DbController::executeSqlConsole('CREATE EXTENSION IF NOT EXISTS pgcrypto');
        } catch (Exception $e) {}

        echo '    > create function blowfish(text) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION blowfish(text)
  RETURNS text AS
\$BODY\$
  SELECT substr(crypt($1, '$2a$08$.t/QsvjgquCzOieDCybNHe'), 30);
\$BODY\$
  LANGUAGE sql IMMUTABLE STRICT
  COST 100;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create type user_roles ...';
        $time = microtime(true);
        $this->db->createCommand('CREATE TYPE user_roles AS ENUM (\'guest\', \'registered\', \'verified\', \'admin\');')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'role' => 'user_roles NOT NULL DEFAULT \'guest\'',

            'title'      => $this->string(95)->notNull(),
            'first_name' => $this->string(63),
            'last_name'  => $this->string(63),

            'timezone' => $this->string(31),

            'email' => $this->string(255),

            'password_hash' => $this->string(255),
            'auth_key'      => $this->string(32),

            'created timestamp with time zone NOT NULL',
            'updated timestamp with time zone NOT NULL',
        ]);

        $this->createIndex(
            $this->itemName . '_idx_role',
            $tableName,
            'role',
            'hash'
        );

        echo '    > create not null index ', $this->itemName, '_idx_email ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE INDEX {$this->itemName}_idx_email
  ON {$tableName}
  USING btree
  (email)
  WHERE email IS NOT NULL;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $this->createIndex(
            $this->itemName . '_idx_title',
            $tableName,
            'title',
            'hash'
        );

        echo '    > create not null unique index ', $this->itemName, '_idx_blowfish_id_and_updated_and_password_hash ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE UNIQUE INDEX {$this->itemName}_idx_blowfish_id_and_updated_and_password_hash
  ON {$tableName}
  USING btree
  (blowfish(((id::text || date_part('epoch'::text, updated AT TIME ZONE 'UTC')) || '_'::text) || COALESCE(password_hash, ''::character varying)::text))
  WHERE password_hash IS NOT NULL;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');

        echo '    > drop type user_roles ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TYPE IF EXISTS user_roles;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function blowfish(text) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS blowfish(text);')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        DbController::executeSqlConsole('DROP EXTENSION IF EXISTS pgcrypto');
    }
}
