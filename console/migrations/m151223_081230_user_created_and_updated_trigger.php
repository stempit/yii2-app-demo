<?php

use yii\db\Migration;

class m151223_081230_user_created_and_updated_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'user';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemName, '_set_created_and_updated() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemName}_set_created_and_updated()
RETURNS TRIGGER AS
\$BODY\$
BEGIN
  IF (TG_OP = 'INSERT') THEN
    NEW.created := NOW();
  ELSE
    IF (
      NEW.role = OLD.role AND
      NEW.title      = OLD.title AND
      NEW.first_name = OLD.first_name AND
      NEW.last_name  = OLD.last_name AND
      NEW.timezone = OLD.timezone AND
      NEW.email = OLD.email
    ) THEN
      RETURN NEW;
    END IF;
  END IF;

  NEW.updated := NOW();
  RETURN NEW;
END
\$BODY\$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > create trigger ', $this->itemName, '_before_insert_or_update_set_created_and_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemName}_before_insert_or_update_set_created_and_updated
BEFORE INSERT OR UPDATE ON {$tableName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemName}_set_created_and_updated();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > drop trigger ', $this->itemName, '_before_insert_or_update_set_created_and_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemName}_before_insert_or_update_set_created_and_updated ON {$tableName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemName, '_set_created_and_updated() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemName}_set_created_and_updated();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
