<?php

namespace console\controllers;

use Yii;
use \yii\console\Controller;
use \yii\helpers\Console;

class EmailController extends Controller
{
    /**
     * @var string|null
     */
    public $to;

    /**
     * @var string|null
     */
    public $subject;

    /**
     * @var string|null
     */
    public $body;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        $options = parent::options($actionID);
        if ($actionID === 'send') {
            $options[] = 'to';
            $options[] = 'subject';
            $options[] = 'body';
        }
        return $options;
    }

    /**
     *
     * @return int
     */
    public function actionSend()
    {
        echo PHP_EOL, 'This ';

        $app = Yii::$app;
        $appParams = $app->params;
        $emailDefault = [$appParams['emailSupport'] => $appParams['brandName']];
        $mailerMessage = $app
            ->getMailer()
            ->compose()
            ->setFrom($emailDefault)
            ->setTo(      $this->to      ? $this->to      : $emailDefault)
            ->setSubject( $this->subject ? $this->subject : 'Test')
            ->setTextBody($this->body    ? $this->body    : 'test');

        $resultMessage = 'email was ';
        if ($mailerMessage->send()) {
            $resultCode = static::EXIT_CODE_NORMAL;
        } else {
            $resultCode = static::EXIT_CODE_ERROR;

            $resultMessage .= 'not ';
        }
        echo $this->ansiFormat($resultMessage . 'sent', Console::BOLD), ':', PHP_EOL, PHP_EOL;

        print_r($mailerMessage->toString());
        echo PHP_EOL;
        return $resultCode;
    }
}
