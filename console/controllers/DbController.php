<?php

namespace console\controllers;

use Yii;
use \DateInterval;
use \yii\console\Controller;
use \yii\console\Exception;

class DbController extends Controller
{
    /**
     * @var string|null|boolean
     */
    public $db;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        $options = parent::options($actionID);
        if ($actionID === 'execute-sql') {
            $options[] = 'db';
        }
        return $options;
    }

    /**
     * @param string $query
     * @param string|null|boolean $db
     * @return array
     * @throws Exception
     */
    public static function executeSqlConsole($query, $db = true)
    {
        $dsn = Yii::$app->getDb()->dsn;
        $dsn = explode(';', substr($dsn, strpos($dsn, ':') + 1));

        $dsnByKey = [];
        foreach ($dsn as $item) {
            $item = explode('=', $item);

            $dsnByKey[$item[0]] = $item[1];
        }

        $command = ' -c "' . str_replace('"', '\"', $query) . '"';
        if (!empty($dsnByKey['host'])) {
            $command = ' -h ' . $dsnByKey['host'] . $command;
        }
        if ($db !== false) {
            if (!is_string($db)) {
                $db = $dsnByKey['dbname'];
            }
            $command = ' -d ' . $db . $command;
        }
        $command ='psql -U postgres ' . $command;
        $streams = [
            ['pipe', 'r'], //stdin
            ['pipe', 'w'], //stdout
            ['pipe', 'w']  //stderr
        ];
        $process = proc_open($command, $streams, $pipes);
        if (!is_resource($process)) {
            throw new Exception('Cannot open process: ' . $command);
        } else {
            list(, $stdout, $stderr) = $pipes;
            $error = stream_get_contents($stderr);
            fclose($stderr);
            if (strlen($error) > 0) {
                throw new Exception('Process was ended with error: ' . $error);
            } else {
                $output = stream_get_contents($stdout);
                fclose($stdout);
                $returnCode = proc_close($process);
                if ($returnCode === -1) {
                    throw new Exception('Process was completed incorrectly: ' . $output);
                } else {
                    return [$returnCode, $output];
                }
            }
        }
    }

    /**
     * Execute SQL as user "postgres" without password. Optional --db=.
     * @param string $query
     * @return int
     * @throws Exception
     */
    public function actionExecuteSql($query)
    {
        if (is_string($this->db)) {
            if ($this->db === 'false' || $this->db === '0') {
                $this->db = false;
            }

            $return = static::executeSqlConsole($query, $this->db);
        } else {
            $return = static::executeSqlConsole($query);
        }

        echo $return[1];
        return $return[0];
    }

    /**
     * Optimizes database.
     * @return int
     * @throws \yii\db\Exception
     */
    public function actionOptimize()
    {
        $connection = Yii::$app->getDb();

        $tables = $connection->createCommand(<<<SQL
SELECT table_name
FROM INFORMATION_SCHEMA.TABLES
WHERE table_schema = 'public';
SQL
        )->queryColumn();
        echo 'Found ', count($tables), ' tables:', PHP_EOL;

        $timeAll = microtime(true);
        foreach ($tables as $table) {
            $timeStepStart = microtime(true);
            echo '* ', $table;

            $sql = 'VACUUM ANALYZE "' . $table . '";';
            $connection->createCommand($sql)->execute();
            echo ' completed';

            $sql = 'REINDEX TABLE  "' . $table . '";';
            $connection->createCommand($sql)->execute();
            echo sprintf(' in %.3f seconds;', microtime(true) - $timeStepStart), PHP_EOL;
        }
        $timeAll = new DateInterval('PT' . ceil(microtime(true) - $timeAll) . 'S');
        echo 'All done in ', $timeAll->format('%H:%I:%S'), '.', PHP_EOL;

        return static::EXIT_CODE_NORMAL;
    }
}
