<?php

namespace common\components;

use Yii;

/**
 * @inheritdoc
 *
 * @property \common\models\User|\yii\web\IdentityInterface|null $identity
 * @method   \common\models\User|\yii\web\IdentityInterface|null getIdentity() getIdentity(boolean $autoRenew = true)
 */
class User extends \yii\web\User
{
}
