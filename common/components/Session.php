<?php
namespace common\components;

use Yii;
use yii\base\InvalidParamException;

/**
 * Session is component with fix setSavePath: support optional argument that determines the number of directory levels.
 */
class Session extends \yii\web\Session
{
    /**
     * @return int how many bits are stored in each character when converting the binary hash data to something readable. The possible values are '4' (0-9, a-f), '5' (0-9, a-v), and '6' (0-9, a-z, A-Z, "-", ",").
     */
    public function getHashBitsPerCharacter()
    {
        return (int) ini_get('session.hash_bits_per_character');
    }

    /**
     * @param int $value how many bits are stored in each character when converting the binary hash data to something readable. The possible values are '4' (0-9, a-f), '5' (0-9, a-v), and '6' (0-9, a-z, A-Z, "-", ",").
     * @throws InvalidParamException if the value is not between 4 and 6.
     */
    public function setHashBitsPerCharacter($value)
    {
        if ($value >= 4 && $value <= 6) {
            ini_set('session.hash_bits_per_character', $value);
        } else {
            throw new InvalidParamException('HashBitsPerCharacter must be a value between 4 and 6.');
        }
    }

    /**
     * @inheritdoc
     */
    public function setSavePath($value)
    {
        $levelPos = strpos($value, ';');
        if ($levelPos === false) {
            $value = $path = Yii::getAlias($value);
        } elseif ($levelPos === 0) {
            throw new InvalidParamException('Directory level in session save path is empty');
        } else {
            $level = substr($value, 0, $levelPos);
            if (!is_numeric($level)) {
                throw new InvalidParamException('Directory level in session save path is not a valid number' . $level);
            }

            $path = Yii::getAlias(substr($value, $levelPos + 1));

            $value = $level . ';' . $path;
            $path .= DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array_fill(0, $level, 'a'));
        }

        if (is_dir($path)) {
            session_save_path($value);
        } else {
            throw new InvalidParamException('Session directory is not a valid directory: ' . $path);
        }
    }
}
