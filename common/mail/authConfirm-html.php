<?php
use common\helpers\Url;
use common\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $username            string
 * @var $password            string
 * @var $showFrontendProfile bool
 */
?>

<p><?= Yii::t('common\email', 'Hello, {username}!', ['username' => Html::encode($username)]); ?></p>

<p>
    <?= Yii::t('common\email', 'Your password on the {appName}: {password}', [
        'appName'  => Html::a(Yii::$app->name, Url::base(true)),
        'password' => Html::tag('strong', $password)
    ]); ?>
</p>

<?php if ($showFrontendProfile) { ?>
<p><?= Yii::t(
    'common\email',
    'Password can be changed in {your_profile}.',
    ['your_profile' => Html::a(
        Yii::t('common', 'your profile'),
        Url::toRouteFrontend(['auth/profile'], true)
    )]
); ?></p>
<?php } ?>

