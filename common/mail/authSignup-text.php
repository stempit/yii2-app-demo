<?php
use common\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $username string
 * @var $token    string
 */
?>

<?= Yii::t('common\email', 'Hello, {username}!', ['username' => $username]); ?> <?= Yii::t('common\email', 'Someone, maybe you, registered on {appName}.', ['appName' => Yii::$app->name]); ?>

<?= Yii::t('common\email', 'To finish the registration, click here:'); ?> <?= Url::toRoute(['auth/confirm', 'token' => $token], true); ?>

<?= Yii::t('common\email', 'If you did not register, ignore this email.'); ?>
