<?php
use common\helpers\Url;
use common\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $username string
 * @var $token    string
 * @var $toFrontend bool
 */

$appName         = $toFrontend ? Yii::$app->params['brandName'] : Yii::$app->name;
$baseFunction    = $toFrontend ?    'baseFrontend' : 'base';
$toRouteFunction = $toFrontend ? 'toRouteFrontend' : 'toRoute';
?>

<p>
    <?= Yii::t('common\email', 'Hello, {username}!', ['username' => Html::encode($username)]); ?>
    <?= Yii::t('common\email', 'You were invited to the {appName}.', ['appName' => Html::a($appName, Url::$baseFunction(true))]); ?>
</p>

<p>
    <?= Yii::t('common\email', 'To get a password, {click here}.', ['click here' => Html::tag('strong', Yii::t('common', 'click {here}', [
        'here' => Html::a(Yii::t('common', 'here'), Url::$toRouteFunction(['auth/confirm', 'token' => $token], true)),
    ]))]); ?>
</p>

<p><?= Yii::t('common\email', 'If you do not have any relationship to this site, ignore this email.'); ?></p>
