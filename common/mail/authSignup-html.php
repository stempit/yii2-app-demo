<?php
use common\helpers\Url;
use common\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $username string
 * @var $token    string
 */
?>

<p>
    <?= Yii::t('common\email', 'Hello, {username}!', ['username' => Html::encode($username)]); ?>
    <?= Yii::t('common\email', 'Someone, maybe you, registered on {appName}.', ['appName' => Html::a(Yii::$app->name, Url::base(true))]); ?>
</p>

<p>
    <?= Yii::t('common\email', 'To finish the registration, {click here}.', ['click here' => Html::tag('strong', Yii::t('common', 'click {here}', [
        'here' => Html::a(Yii::t('common', 'here'), Url::toRoute(['auth/confirm', 'token' => $token], true)),
    ]))]); ?>
</p>

<p><?= Yii::t('common\email', 'If you did not register, ignore this email.'); ?></p>
