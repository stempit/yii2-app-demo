<?php
use common\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $username            string
 * @var $password            string
 * @var $showFrontendProfile bool
 */
?>

<?= Yii::t('common\email', 'Hello, {username}!', ['username' => $username]); ?>

<?= Yii::t('common\email', 'Your password on the {appName}: {password}', [
    'appName'  => Yii::$app->name,
    'password' => $password
]); ?>

<?php if ($showFrontendProfile) { ?>
<?= Yii::t(
    'common\email',
    'Password can be changed in your profile: {url}.',
    ['url' => Url::toRouteFrontend(['auth/profile'], true)]
); ?>
<?php } ?>
