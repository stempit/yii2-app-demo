<?php

namespace common\helpers;

use Yii;
use yii\helpers\BaseStringHelper;

/**
 * StringHelper is helper extends BaseStringHelper.
 *
 * @author Kostya M <iamF13@ya.ru>
 */
class StringHelper extends BaseStringHelper
{

    const PUNCTUATION_MARKS = '-‑—.…,:;!?"\'<>()«»„“';

    const WHITESPACES = "  \t\n\r\0";

    const NULLBYTE_CHARACTER = '{\0}';

    /**
     * Multibyte trim.
     *
     * @param string $string
     * @param string $char_list
     * @return string
     */
    public static function mb_trim($string, $char_list = "  \t\n\r\0")
    {
        $char_list = preg_quote($char_list);
        return preg_replace('/^[' . $char_list . ']*(?U)(.*)[' . $char_list . ']*$/u', '\\1', $string);
    }

    /**
     * Multibyte str_replace.
     *
     * @param string|array $search
     * @param string|array $replace
     * @param string|array $subject
     * @param int $count
     * @return string|boolean
     */
    public static function mb_str_replace($search, $replace, $subject, &$count = 0)
    {
        if (!is_array($search) && is_array($replace)) {
            return false;
        }
        if (is_array($subject)) {
            // call mb_replace for each single string in $subject
            foreach ($subject as &$string) {
                $string = static::mb_str_replace($search, $replace, $string, $c);
                $count += $c;
            }
        } elseif (is_array($search)) {
            if (!is_array($replace)) {
                foreach ($search as $string) {
                    $subject = static::mb_str_replace($string, $replace, $subject, $c);
                    $count += $c;
                }
            } else {
                $n = max(count($search), count($replace));
                while ($n--) {
                    $subject = static::mb_str_replace(current($search), current($replace), $subject, $c);
                    $count += $c;
                    next($search);
                    next($replace);
                }
            }
        } else {
            $parts = mb_split(preg_quote($search), $subject);
            $count = count($parts) - 1;
            $subject = implode($replace, $parts);
        }
        return $subject;
    }

    /**
     * Multibyte lcfirst.
     *
     * @param string $str
     * @return string
     */
    public static function mb_lcfirst($str)
    {
        return mb_strtolower(mb_substr($str, 0, 1)) . mb_substr($str, 1);
    }

    /**
     * Prepare string to be stored in PostgreSQL.
     * PostgreSQL does not support null byte and characters not from UTF-8 range.
     *
     * @param string $str
     * @return string
     */
    public static function pg_text_store($str)
    {
        $str = static::mb_str_replace("\0", static::NULLBYTE_CHARACTER, $str);

        return static::replaceNonUtf8($str);
    }

    /**
     * Returns string before processing pg_text_store.
     *
     * @param $str
     * @return string
     */
    public static function pg_text_get($str)
    {
        return static::mb_str_replace(static::NULLBYTE_CHARACTER, "\0", $str);
    }

    /**
     * @param string $str
     * @param integer|string $replaceChar
     * @return string
     */
    public static function replaceNonUtf8($str, $replaceChar = 'none')
    {
        mb_substitute_character($replaceChar);
        return mb_convert_encoding($str, 'UTF-8', 'UTF-8');
    }

    /**
     * @param string $string The string to truncate.
     * @param integer $length How many characters from original string to include into truncated string.
     * @param string $suffix String to append to the end of truncated string.
     * @param string $encoding The charset to use, defaults to charset currently used by application.
     * @return string the truncated string.
     */
    public static function truncateAtWordEnd($string, $length, $suffix = '...', $encoding = null)
    {
        if (mb_strlen($string, $encoding ?: Yii::$app->charset) > $length) {
            $string = mb_substr($string, 0, $length, $encoding ?: Yii::$app->charset);

            $marksWordEnd = static::WHITESPACES . static::PUNCTUATION_MARKS;
            $marksWordEndQuoted = preg_quote($marksWordEnd);
            $string = preg_replace('/[' . $marksWordEndQuoted . '][^' . $marksWordEndQuoted . ']*?$/u', '', $string, 1);

            return static::mb_trim($string, $marksWordEnd) . $suffix;
        } else {
            return $string;
        }
    }

    /**
     * @param int $minLength
     * @param int $maxLength
     * @param bool $useNumbers
     * @return string
     */
    public static function generateRememberedString($minLength = 6, $maxLength = 8, $useNumbers = true)
    {
        $consonant = ['b', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'y', 'z'];
        $vowel     = ['a', 'e', 'i', 'o', 'u'];
        $consonantLast = count($consonant) - 1;
        $vowelLast     = count($vowel) - 1;

        $password = '';
        for ($i = 0, $count = rand($minLength, $maxLength); $i < $count; ++$i) {
            if ($i % 2 == 0) {
                $password .= $consonant[rand(0, $consonantLast)];
            } else {
                $password .=     $vowel[rand(0, $vowelLast)];
            }
        }

        if ($useNumbers) {
            $number = range(0, 9);
            $numberLast = count($number) - 1;

            $minLength = round($minLength / 3);
            $maxLength = round($maxLength / 2);

            for ($i = 0, $count = rand($minLength, $maxLength); $i < $count; ++$i) {
                $password .= $number[rand(0, $numberLast)];
            }
        }

        return $password;
    }

    /**
     * @param null|string|float|int $string
     * @return null|string
     */
    public static function getNumber($string)
    {
        if ($string === null) {
            return null;
        }
        if (is_int($string) || is_float($string)) {
            return (string) $string;
        }

        $symbolsForClear = str_split(static::WHITESPACES);
        $symbolsForClear[] = '$';

        $string = str_replace($symbolsForClear, '', $string);
        return is_numeric($string) ? $string : null;
    }

    /**
     * @param null|string|int $string
     * @return null|int
     */
    public static function getInt($string)
    {
        if (is_int($string)) {
            return $string;
        }

        $string = static::getNumber($string);
        return ($string === null) ? null : (int) $string;
    }

    /**
     * @param null|string $row
     * @return null|string
     */
    public static function filterFromHtml($row)
    {
        if ($row === null) {
            return null;
        }

        return preg_replace('/[ ]{2,}/', ' ', trim(str_replace(["\n", "\r"], ' ', $row)));
    }

    /**
     * @param string|integer $value
     * @return string
     */
    public static function asDate($value)
    {
        if (!is_numeric($value)) {
            $value = strtotime($value);
        }

        return date('Y-m-d', $value);
    }

    /**
     * @param string|integer $value
     * @return string
     */
    public static function asDateTime($value)
    {
        if (!is_numeric($value)) {
            $value = strtotime($value);
        }

        return date('Y-m-d H:i:s', $value);
    }

    /**
     * @param string|integer $value
     * @return string
     */
    public static function asDateTimeWithTZ($value)
    {
        if (!is_numeric($value)) {
            $value = strtotime($value);
        }

        $value = date('Y-m-d H:i:sO', $value);
        if (substr($value, -2) === '00') {
            $value = substr($value, 0, -2);
        }
        return $value;
    }
}
