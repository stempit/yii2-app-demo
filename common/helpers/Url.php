<?php

namespace common\helpers;

use Yii;
use yii\helpers\BaseUrl;
use yii\base\InvalidConfigException;

/**
 * Url is helper extends BaseUrl.
 *
 * @author Kostya M <iamF13@ya.ru>
 */
class Url extends BaseUrl
{
    /**
     * @return \yii\web\UrlManager
     * @throws InvalidConfigException
     */
    protected static function getUrlManager()
    {
        $app = Yii::$app;
        if ($app->has('urlManager', true)) {
            return $app->get('urlManager');
        } elseif ($app->has('frontendUrlManager')) {
            return $app->get('frontendUrlManager');
        } else {
            throw new InvalidConfigException('Url manager is not found');
        }
    }

    /**
     * @return \yii\web\UrlManager
     * @throws InvalidConfigException
     */
    protected static function getUrlManagerFrontend()
    {
        $app = Yii::$app;
        if ($app->has('frontendUrlManager')) {
            return $app->get('frontendUrlManager');
        } elseif ($app->has('urlManager', true)) {
            return $app->get('urlManager');
        } else {
            throw new InvalidConfigException('Frontend url manager is not found');
        }
    }

    /**
     * @param \yii\web\UrlManager $urlManager
     * @param string|array $route
     * @param boolean|string $scheme
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    protected static function toRouteByUrlManager($urlManager, $route, $scheme = false)
    {
        // code from BaseUrl::toRoute
        $route = (array) $route;
        $route[0] = static::normalizeRoute($route[0]);

        return $scheme ?
            $urlManager->createAbsoluteUrl($route, is_string($scheme) ? $scheme : null) :
            $urlManager->createUrl($route);
    }

    /**
     * @param \yii\web\UrlManager $urlManager
     * @param boolean|string $scheme
     * @return string
     * @throws InvalidConfigException
     */
    public static function baseByUrlManager($urlManager, $scheme = false)
    {
        // code from BaseUrl::base
        $url = $urlManager->getBaseUrl();
        if ($scheme) {
            $url = $urlManager->getHostInfo() . $url;
            if (is_string($scheme) && ($pos = strpos($url, '://')) !== false) {
                $url = $scheme . substr($url, $pos);
            }
        }
        return $url;
    }

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public static function toRoute($route, $scheme = false)
    {
        return static::toRouteByUrlManager(
            static::getUrlManager(),
            $route,
            $scheme
        );
    }

    /**
     * @param string|array $route
     * @param boolean|string $scheme
     * @return string
     * @throws InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function toRouteFrontend($route, $scheme = false)
    {
        return static::toRouteByUrlManager(
            static::getUrlManagerFrontend(),
            $route,
            $scheme
        );
    }

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public static function base($scheme = false)
    {
        return static::baseByUrlManager(
            static::getUrlManager(),
            $scheme
        );
    }

    /**
     * @param boolean|string $scheme
     * @return string
     * @throws InvalidConfigException
     */
    public static function baseFrontend($scheme = false)
    {
        return static::baseByUrlManager(
            static::getUrlManagerFrontend(),
            $scheme
        );
    }
}
