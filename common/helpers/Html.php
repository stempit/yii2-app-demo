<?php

namespace common\helpers;

use yii\helpers\BaseHtml;

class Html extends BaseHtml
{
    /**
     * @param string $name
     * @param string|array $selection
     * @param array $items
     * @param array $options
     * @return string
     */
    public static function checkboxListWithHeading($name, $selection, $items = [], $options = [])
    {
        if (substr($name, -2) !== '[]') {
            $name .= '[]';
        }

        $formatter = isset($options['item']) ? $options['item'] : null;
        $headingTag = isset($options['headingTag']) ? $options['headingTag'] : 'legend';
        $separator = isset($options['separator']) ? $options['separator'] : '';
        $itemOptions = isset($options['itemOptions']) ? $options['itemOptions'] : [];
        $encode = !isset($options['encode']) || $options['encode'];

        $lines = [];
        $index = 0;
        foreach ($items as $groupName => $item) {
            $lines[] = Html::tag($headingTag,  $groupName);
            foreach ($item as $value => $label) {
                $checked = $selection !== null &&
                    (!is_array($selection) && !strcmp($value, $selection)
                        || is_array($selection) && in_array($value, $selection));
                if ($formatter !== null) {
                    $lines[] = call_user_func($formatter, $index, $label, $name, $checked, $value);
                } else {
                    $lines[] = static::checkbox($name, $checked, array_merge($itemOptions, [
                        'value' => $value,
                        'label' => $encode ? static::encode($label) : $label,
                    ]));
                }
                $lines[] = $separator;
                $index++;
            }
        }

        if (isset($options['unselect'])) {
            $name2 = substr($name, -2) === '[]' ? substr($name, 0, -2) : $name;
            $hidden = static::hiddenInput($name2, $options['unselect']);
        } else {
            $hidden = '';
        }

        $tag = isset($options['tag']) ? $options['tag'] : 'div';
        unset($options['tag'], $options['unselect'], $options['encode'], $options['separator'], $options['item'], $options['itemOptions'], $options['headingTag']);

        return $hidden . static::tag($tag, implode("\n", $lines), $options);
    }

    /**
     * @param string $string
     * @return string
     */
    public static function encodeYandex($string)
    {
        return htmlspecialchars(str_replace("\r", '', $string), ENT_QUOTES | ENT_XML1, \Yii::$app->charset, false);
    }

    /**
     * @param string|\DOMElement $html
     * @return string
     */
    public static function getText($html)
    {
        if (is_object($html) && $html instanceof \DOMElement) {
            $html = $html->ownerDocument->saveHTML($html);
        }
        /** @var string $html */

        return trim(
            html_entity_decode(
                strip_tags(
                    str_replace(
                        '><',
                        '> <',
                        preg_replace(
                            '/\<(?:\/(?:div|p)|br[^>]*+)\>/i',
                            "$0\n",
                            $html
                        )
                    )
                ),
                ENT_QUOTES | ENT_HTML5
            )
        );
    }
}
