<?php
$yiiDir = __DIR__ . '/../vendor/yiisoft/yii2/';

/**
 * Yii bootstrap file.
 *
 * @link http://www.yiiframework.com/
 */

require($yiiDir . 'BaseYii.php');

/**
 * Yii is a helper class serving common framework functionalities.
 *
 * It extends from [[\yii\BaseYii]] which provides the actual implementation.
 * By writing your own Yii class, you can customize some functionalities of [[\yii\BaseYii]].
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication
     */
    public static $app;
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = require($yiiDir . 'classes.php');
Yii::$container = new yii\di\Container();

/**
 * Class BaseApplication
 * Used for properties that are identical for both WebApplication and ConsoleApplication
 *
 * @property \yii\caching\Cache                      $cacheCommon
 * @property \frontend\components\FrontendUrlManager $frontendUrlManager
 */
abstract class BaseApplication extends yii\base\Application
{
}

/**
 * Class WebApplication
 * Include only Web application related components here
 *
 * @property \common\components\User $user
 * @method   \common\components\User getUser()
 */
class WebApplication extends yii\web\Application
{
}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 */
class ConsoleApplication extends yii\console\Application
{
}
