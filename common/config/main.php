<?php
return [
    'timeZone' => 'Europe/Moscow',
    'language' => 'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cacheCommon' => [
            'keyPrefix' => '',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 0,
            'schemaCache' => 'cacheCommon',
            'charset' => 'UTF-8',
            'on afterOpen' => function($event) {
                $event->sender->createCommand('SET timezone = \'' . \Yii::$app->timeZone . '\';')->execute();
            },
        ],
        'log' => [
            'targets' => [
                'errorCommon' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/error.log',
                    'rotateByCopy' => strtoupper(substr(PHP_OS, 0, 3)) === 'WIN',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'messageConfig' => [
                'charset' => 'UTF-8',
            ],
        ],
        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
    ],
];
