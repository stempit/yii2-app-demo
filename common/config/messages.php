<?php

return [
    'sourcePath' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..',
    'only' => ['*.php'],
    'except' => [
        '.idea',
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/vendor',
        '/tests',
    ],
    'ignoreCategories' => [
        'yii',
    ],

    'messagePath' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'messages',
    'format' => 'php',
    'languages' => ['ru-RU'],
    'overwrite' => true,
];
