<?php
$subDomainFrontend = '';
$subDomainBackend  = 'admin';
$subDomainApi      = 'api';

if (array_key_exists('SERVER_NAME', $_SERVER)) {
    $domain = $_SERVER['SERVER_NAME'];
    if ($subDomainFrontend && strpos($domain, $subDomainFrontend) === 0) {
        $domain = substr($domain, strlen($subDomainFrontend) + 1);
    } elseif (strpos($domain, $subDomainBackend) === 0) {
        $domain = substr($domain, strlen($subDomainBackend) + 1);
    }
} else {
    $domain = 'blabla.com';
}

$frontendDomain = $domain;
if ($subDomainFrontend) {
    $frontendDomain = $subDomainFrontend . '.' . $frontendDomain;
}

return [
    'frontendScheme' => 'http',
    'frontendDomain' => $frontendDomain,
    'backendDomain'  => $subDomainBackend . '.' . $domain,
    'apiDomain'      => $subDomainApi     . '.' . $domain,
    'brandName' => 'BlaBla',
    'emailInfo'    =>    'info@' . $domain,
    'emailSupport' => 'support@' . $domain,
    'emailBugs'    =>    'bugs@' . $domain,
];
