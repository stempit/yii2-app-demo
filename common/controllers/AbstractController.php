<?php
namespace common\controllers;

use yii\web\Controller;

/**
 * Base Controller.
 */
abstract class AbstractController extends Controller
{
    /**
     * @return string model class
     */
    public function getBaseModelClass()
    {
        $modelClass = static::className();

        $pos = strrpos($modelClass, '\\');
        if ($pos !== false) {
            $modelClass = substr($modelClass, $pos + 1);
        }

        if (substr($modelClass, -10) === 'Controller') {
            $modelClass = substr($modelClass, 0, -10);
        }

        return $modelClass;
    }
}
