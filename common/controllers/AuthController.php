<?php

namespace common\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\helpers\StringHelper;
use common\models\User;
use common\models\LoginForm;
use common\models\PasswordNewRequestForm;

class AuthController extends Controller
{
    /**
     * @var string|array
     */
    protected static $urlAfterConfirm = '/';

    /**
     * @param null|string $redirect
     * @return \yii\web\Response
     */
    public function actionLogout($redirect = null)
    {
        $user = Yii::$app->getUser();
        if (!$user->getIsGuest()) {
            $user->logout();
        }

        return $this->goBack($redirect);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function actionRequestPasswordNew()
    {
        $model = new PasswordNewRequestForm();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Further instructions sent to your email.'));

                return $this->redirect(['auth/login']);
            } else {
                //TODO: this message should be sent to the render (by validation error?), it's not necessary to do this through the session
                Yii::$app->getSession()->setFlash('error', Yii::t('common', 'Email send failed. Please try again later.'));
            }
        }

        return $this->render('requestPasswordNew', [
            'model' => $model,
        ]);
    }

    /**
     * @param string $token
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function actionConfirm($token)
    {
        $user = User::find()->where([User::COLUMN_TOKEN => $token])->andWhere('password_hash IS NOT NULL')->one();
        if ($user === null) {
            throw new NotFoundHttpException(Yii::t('common', 'User with this token is not found.'));
        }

        $password = StringHelper::generateRememberedString();

        $user->setPassword($password);
        $user->generateAuthKey();
        if ($user->role === 'guest' || $user->role === 'registered') {
            $user->role = 'verified';
        }
        $user->save(false);

        $app = Yii::$app;
        $app->getUser()->login($user);

        if ($app->getMailer()
            ->compose(
                [
                    'html' => 'authConfirm-html',
                    'text' => 'authConfirm-text',
                ],
                [
                    'username' => $user->title,
                    'password' => $password,
                    'showFrontendProfile' => true,
                ]
            )
            ->setTo($user->email)
            ->setSubject(Yii::t('common', 'Password'))
            ->send()
        ) {
            $app->getSession()->setFlash('success', Yii::t('common', 'Your password sent to the email.'));
        } else {
            $app->getSession()->setFlash('warning', Yii::t('common', 'Email with password send failed. You can set a new one or try to request again.'));
        }

        return $this->redirect(static::$urlAfterConfirm);
    }
}
