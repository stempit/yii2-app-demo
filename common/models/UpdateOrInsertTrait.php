<?php

namespace common\models;

use Yii;
use \yii\base\InvalidParamException;
use \yii\db\ActiveQuery;

/**
 * Trait adds convenient methods.
 */
trait UpdateOrInsertTrait
{
    /**
     * @param array $attributes
     * @return static
     * @throws InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public static function getForSave($attributes)
    {
        /** @var string[] $primaryKeys */
        $primaryKeys = static::primaryKey();

        if (array_diff($primaryKeys, array_keys($attributes)) !== []) {
            throw new InvalidParamException('Primary key must be contained in attributes.');
        }

        $columns = static::getTableSchema()->columns;

        foreach ($primaryKeys as $key) {
            if ($columns[$key]->type === 'bigint') {
                $attributes[$key] = (string) $attributes[$key];
            }
        }

        /** @var ActiveQuery $itemQuery */
        $itemQuery = static::find();
        foreach ($primaryKeys as $key) {
            $itemQuery->andWhere([$key => $attributes[$key]]);
        }
        $item = $itemQuery->one();
        if ($item === null) {
            $item = new static;
        }

        $item->setScenario('administrator');
        $item->setAttributes($attributes);

        return $item;
    }

    /**
     */
    public function logValidationErrors()
    {
        $errors = $this->getErrors();
        if ($errors === []) {
            return;
        }

        $className = get_class($this);
        Yii::error(
            'Validation errors on save ' . $className . PHP_EOL .
                print_r($this->getAttributes(), true) . PHP_EOL .
                print_r($errors,                true),
            $className . '\validation\\' . implode('-', array_values($this->getPrimaryKey(true)))
        );
    }

    /**
     * @param array $attributes
     * @return boolean|integer
     * @throws InvalidParamException
     */
    public static function updateOrInsert($attributes)
    {
        $item = static::getForSave($attributes);
        if ($item->save() === false) {
            $item->logValidationErrors();
            return false;
        }
        return $item->getPrimaryKey();
    }

    /**
     * @param array $attributes
     * @return array
     */
    public static function normalizeAttributes($attributes)
    {
        if (method_exists(get_called_class(), 'getNormalizeAttributeMap')) {
            foreach (static::getNormalizeAttributeMap() as $normalizedName => $actualName) {
                $attributes[$normalizedName] = call_user_func($actualName, $attributes);
            }
        }
        return $attributes;
    }

    /**
     * @param array $items
     * @return int[]|string[]
     * @throws InvalidParamException
     */
    public static function updateOrInsertMultiple($items)
    {
        $ids = [];
        foreach ($items as $item) {
            $id = static::updateOrInsert(static::normalizeAttributes($item));
            if ($id !== false) {
                $ids[] = $id;
            }
        }
        return $ids;
    }

    /**
     * @param array $primaryKeysForChecking
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getNotExist($primaryKeysForChecking)
    {
        $primaryKeysAsRecords = $primaryKeysForChecking;
        array_walk($primaryKeysAsRecords, function (&$value) {
            if (is_array($value)) {
                $value = implode(', ', $value);
            }

            $value = '(' . $value . ')';
        });
        $primaryKeysAsRecords = implode(', ', $primaryKeysAsRecords);

        /** @var string[] $primaryKeyColumns */
        $primaryKeyColumns = static::primaryKey();
        $primaryKeyColumnsAsString = implode(', ', $primaryKeyColumns);
        $primaryKeyColumnFirst = $primaryKeyColumns[0];

        $tableName = static::tableName();

        $joinCondition = [];
        foreach ($primaryKeyColumns as $column) {
            $joinCondition[] = 'checking.' . $column . ' = ' . $tableName . '.' . $column;
        }
        $joinCondition = implode(' AND ', $joinCondition);

        $result = Yii::$app->getDb()->createCommand(<<<SQL
WITH checking ({$primaryKeyColumnsAsString}) AS (
  VALUES {$primaryKeysAsRecords}
)
SELECT checking.*
FROM checking
LEFT JOIN {$tableName} ON {$joinCondition}
WHERE {$tableName}.{$primaryKeyColumnFirst} IS NULL
SQL
        );
        return count($primaryKeyColumns) === 1 ? $result->queryColumn() : $result->queryAll();
    }
}
