<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\validators\DatePgValidator;
use yii\base\NotSupportedException;

/**
 * This is the model class for table "user".
 * @package common\models
 *
 * @property integer     $id
 * @property string      $role
 * @property string      $title
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $timezone
 * @property string|null $email
 * @property string|null $password_hash
 * @property string|null $auth_key
 * @property string|null $created
 * @property string|null $updated
 *
 * @property string $password write-only password
 * @property string $passwordNew
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * User friendly english label.
     */
    const LABEL_SINGULAR_en_US = 'User';

    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Пользователь';

    /**
     * User friendly english label.
     */
    const LABEL_PLURAL_en_US = 'Users';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Пользователи';

    /**
     * @var array
     */
    public static $rolesEmailVerified = [
        'verified',
        'admin',
    ];

    /**
     * @var array
     */
    public static $rolesBackend = [
        'admin',
    ];

    /**
     * SQL expression for token.
     */
    const COLUMN_TOKEN = 'blowfish(id::TEXT || date_part(\'epoch\', updated AT TIME ZONE \'UTC\') || \'_\' || COALESCE(password_hash, \'\'))';

    /**
     * @var string|null
     */
    public $token;

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function rules()
    {
        return [
            [['email'], 'filter', 'filter' => 'trim'],
            [['email'], 'filter', 'filter' => 'mb_strtolower'],
            [['title', 'first_name', 'last_name'], 'filter', 'filter' => function ($value) {
                /* @var string $value */
                return \common\helpers\StringHelper::filterFromHtml($value);
            }],

            [['role', 'title'], 'required'],

            [['timezone'],                'string', 'max' => 31],
            [['auth_key'],                'string', 'max' => 32],
            [['first_name', 'last_name'], 'string', 'max' => 63],
            [['passwordNew'],             'string', 'max' => 72, 'min' => 6],
            [['title'],                   'string', 'max' => 95],
            [['email', 'password_hash'],  'string', 'max' => 255],
            [['email'], 'email', 'enableIDN' => true],
            [['created', 'updated'], DatePgValidator::className()],

            [['role'], 'in', 'range' => static::dictionaryRole()],
            ['email', 'unique'],
            [
                'title',
                'unique',
                'filter' => function($query) {
                    /* @var \common\models\UserQuery $query */
                    $query->emailVerified();
                },
            ],

            [['role'], 'default', 'value' => 'guest'],
            [
                [
                    'first_name',
                    'last_name',
                    'timezone',
                    'email',
                    'password_hash',
                    'auth_key',
                ],
                'default',
                'value' => null,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        $rules = [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
            'signup'          => ['password_hash', 'role', 'email', 'title'],
            'profile'         => ['passwordNew',                    'title'],
        ];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'role'          => Yii::t('common', 'Role'),
            'title'         => Yii::t('common', 'Title'),
            'first_name'    => Yii::t('common', 'First name'),
            'last_name'     => Yii::t('common', 'Last name'),
            'timezone'      => Yii::t('common', 'Timezone'),
            'email'         => 'Email',
            'passwordNew'   => Yii::t('common', 'New password'),
            'password_hash' => Yii::t('common', 'Password hash'),
            'auth_key'      => Yii::t('common', 'Auth key'),
            'created'       => Yii::t('common', 'Created'),
            'updated'       => Yii::t('common', 'Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        unset(
            $fields['role'],
            $fields['password_hash'],
            $fields['auth_key'],
            $fields['created'],
            $fields['updated']
        );
        return $fields;
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert && $this->getScenario() === 'administrator') {
                $this->setPassword(\Yii::$app->getSecurity()->generateRandomString());
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->getScenario() === 'administrator') {
            static::sendEmailInvite($this->email);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->select([
                'id',
                'role',
                'title',
                'email',

                'password_hash',
                'auth_key',
            ])
            ->where(['id' => $id])
            ->one();
    }

    /**
     * @inheritdoc
     * @throws \yii\base\NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @return string
     */
    public function getPasswordNew()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param string $password
     * @return boolean
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model.
     *
     * @param string $password
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * @param string $password
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function setPasswordNew($password)
    {
        if ($password) {
            $this->setPassword($password);
        }
    }

    /**
     * Generates "remember me" key.
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function sendEmailInvite($email)
    {
        $user = User::find()
            ->tokenByEmail($email)
            ->addSelect('title, role')
            ->one();

        return Yii::$app->getMailer()
            ->compose(
                [
                    'html' => 'authInvite-html',
                    'text' => 'authInvite-text',
                ],
                [
                    'username' => $user->title,
                    'token'    => $user->token,
                    'toFrontend' => !in_array($user->role, static::$rolesBackend, true),
                ]
            )
            ->setTo($email)
            ->setSubject(Yii::t('common', 'Invitation'))
            ->send();
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public static function dictionaryRole()
    {
        return Yii::$app->getDb()->createCommand('SELECT unnest(enum_range(NULL::user_roles))')->queryColumn();
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public static function dictionaryRoleLabels()
    {
        $items = static::dictionaryRole();
        return array_combine($items, $items);
    }
}
