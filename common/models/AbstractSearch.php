<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\base\UnknownPropertyException;

/**
 * Base Search Model.
 */
abstract class AbstractSearch extends Model
{

    /**
     * @return string model class
     * @throws UnknownPropertyException if the model cannot be found
     */
    public function getBaseModelClass()
    {
        $modelClass = static::className();

        $pos = strrpos($modelClass, '\\');
        if ($pos !== false) {
            $modelClass = substr($modelClass, $pos + 1);
        }

        if (substr($modelClass, -6) === 'Search') {
            $modelClass = substr($modelClass, 0, -6);
        }

        return $modelClass;
    }

    /**
     * Get dataProvider by search params.
     * @param array $params
     * @return \yii\data\ActiveDataProvider
     */
    public abstract function search($params);

    /**
     * Add equal condition in ActiveQuery.
     * @param \yii\db\ActiveQuery $query
     * @param string $attribute
     */
    protected function addConditionEqual($query, $attribute)
    {
        $value = $this->{$attribute};
        if (trim($value) === '') {
            return;
        }

        $query->andWhere([$attribute => $value]);
    }

    /**
     * Add like condition in ActiveQuery.
     * @param \yii\db\ActiveQuery $query
     * @param string $attribute
     */
    protected function addConditionLike($query, $attribute)
    {
        $value = $this->{$attribute};
        if (!$value) {
            return;
        }

        $param = ':' . $attribute;
        $query->andWhere('UPPER(' . $attribute . ') LIKE ' . $param, [$param => '%' . mb_strtoupper($value, Yii::$app->charset) . '%']);
    }

    /**
     * Add more condition in ActiveQuery.
     * @param \yii\db\ActiveQuery $query
     * @param array $attributesCondition
     * @param string $attributeValue
     */
    protected function addConditionLikeMultiple($query, $attributesCondition, $attributeValue)
    {
        $value = $this->{$attributeValue};
        if (!$value) {
            return;
        }

        $attributeValueLike  = ':' . $attributeValue . '_like';
        $attributeValueEqual = ':' . $attributeValue . '_equal';

        $where = ['OR'];
        $params = [];
        foreach ($attributesCondition as $attribute) {
            $notIp = strpos($attribute, '_ip') === false;
            if ($notIp && strpos($attribute, '_id') === false) {
                $where[] = 'UPPER(' . $attribute . ') LIKE ' . $attributeValueLike;
                if (!array_key_exists($attributeValueLike, $params)) {
                    $params[$attributeValueLike] = '%' . mb_strtoupper($value, Yii::$app->charset) . '%';
                }
            } else {
                if ($notIp !== false && preg_match('/^((\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])$/', $value) === 0) {
                    continue;
                }
                $where[] = $attribute . ' = ' . $attributeValueEqual;
                if (!array_key_exists($attributeValueEqual, $params)) {
                    $params[$attributeValueEqual] = $value;
                }
            }
        }
        $query->andWhere($where, $params);

        // TODO: search by relations
        //foreach ($relations as $attribute) {
        //    $query->innerJoinWith($attribute);
        //}
    }

    /**
     * Add compare condition in ActiveQuery.
     * @param \yii\db\ActiveQuery $query
     * @param string $attributeCondition
     * @param string $attributeValue
     * @param string $compare
     */
    protected function addConditionCompare($query, $attributeCondition, $attributeValue, $compare)
    {
        $value = $this->{$attributeValue};
        if ($value === null || $value === '') {
            return;
        }

        $attributeValue = ':' . $attributeValue;
        $query->andWhere($attributeCondition . ' ' . $compare . ' ' . $attributeValue, [$attributeValue => $value]);
    }

    /**
     * Add less or equal condition in ActiveQuery.
     * @param \yii\db\ActiveQuery $query
     * @param string $attributeCondition
     * @param string|null $attributeValue
     */
    protected function addConditionLess($query, $attributeCondition, $attributeValue = null)
    {
        if ($attributeValue === null) {
            $attributeValue = $attributeCondition . '_max';
        }

        $this->addConditionCompare($query, $attributeCondition, $attributeValue, '<=');
    }

    /**
     * Add less condition in ActiveQuery.
     * @param \yii\db\ActiveQuery $query
     * @param string $attributeCondition
     * @param string|null $attributeValue
     */
    protected function addConditionLessNotEqual($query, $attributeCondition, $attributeValue = null)
    {
        if ($attributeValue === null) {
            $attributeValue = $attributeCondition . '_max';
        }

        $this->addConditionCompare($query, $attributeCondition, $attributeValue, '<');
    }

    /**
     * Add more or equal condition in ActiveQuery.
     * @param \yii\db\ActiveQuery $query
     * @param string $attributeCondition
     * @param string|null $attributeValue
     */
    protected function addConditionMore($query, $attributeCondition, $attributeValue = null)
    {
        if ($attributeValue === null) {
            $attributeValue = $attributeCondition . '_min';
        }

        $this->addConditionCompare($query, $attributeCondition, $attributeValue, '>=');
    }

    /**
     * Add in array condition in ActiveQuery.
     * @param \yii\db\ActiveQuery $query
     * @param string $attribute
     */
    protected function addConditionIsContained($query, $attribute)
    {
        $value = $this->{$attribute};
        if (!$value) {
            return;
        }

        if (!is_numeric($value)) {
            $value = '"' . str_replace('"', '\\"', $value) . '"';
        }
        $query->andWhere('\'{' . $value . '}\' <@ ' . $attribute);
    }
}
