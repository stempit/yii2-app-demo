<?php
namespace common\models;

use yii\base\Model;

class PasswordNewRequestForm extends Model
{
    /**
     * @var string|null
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'filter', 'filter' => 'mb_strtolower'],

            ['email', 'required'],
            ['email', 'string', 'max' => 255],
            ['email', 'email', 'enableIDN' => true],

            [
                'email',
                'exist',
                'targetClass' => '\common\models\User',
                'message' => \Yii::t('common', 'User with this email is not found, please register.')
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function sendEmail()
    {
        // if guest without password
        $user = User::find()
            ->select([
                'id',
                'password_hash',
            ])
            ->where(['email' => $this->email])
            ->one();
        if (!$user->password_hash) {
            $user->setPassword(\Yii::$app->getSecurity()->generateRandomString());
            $user->save(false);
        }

        $user = User::find()
            ->tokenByEmail($this->email)
            ->addSelect('title')
            ->one();

        return \Yii::$app->getMailer()
            ->compose(
                [
                    'html' => 'authPasswordNewRequest-html',
                    'text' => 'authPasswordNewRequest-text',
                ],
                [
                    'username' => $user->title,
                    'token'    => $user->token,
                ]
            )
            ->setTo($this->email)
            ->setSubject(\Yii::t('common', 'Request new password'))
            ->send();
    }
}
