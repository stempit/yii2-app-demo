<?php
namespace common\validators;

use Yii;
use yii\validators\RegularExpressionValidator;

/**
 * DatePgValidator validates that the attribute value is a valid postgresql timestamp.
 */
class DatePgValidator extends RegularExpressionValidator
{
    /**
     * @inheritdoc
     */
    public $pattern = '/^(19|20)?\d{2}[\/\-\.]?(0[1-9]|1[0-2])[\/\-\.]?(0[1-9]|[12]\d|3[01])(([ \t]+|T)(0?\d|1\d|2[0-3])\:[0-5]\d(\:[0-5]\d(\.\d+)?)?([+-](0?\d|1[0-2])(\:[0-5]\d)?)?)?$/';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->message = Yii::t('common', '{attribute} must contain date and optionally time.');

        parent::init();
    }
}
