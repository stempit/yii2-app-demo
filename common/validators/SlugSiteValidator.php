<?php
namespace common\validators;

use Yii;
use yii\validators\RegularExpressionValidator;

class SlugSiteValidator extends RegularExpressionValidator
{
    /**
     * @inheritdoc
     */
    public $pattern = '/^[a-z0-9][a-z0-9\-]*[a-z0-9]$/';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->message = Yii::t('common', '{attribute} must contain only small latin letters, numbers and hyphens.');

        parent::init();
    }
}
