<?php

namespace common\validators;

use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\validators\Validator;

/**
 * EachValidator that allows to add error to another attribute
 */
class EachValidator extends \yii\validators\EachValidator
{
    /**
     * @var string attribute add message to
     */
    public $attributeFor;

    /**
     * @inheritdoc
     */
    private $_validator;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    private function getValidator($model = null)
    {
        if ($this->_validator === null) {
            $this->_validator = $this->createEmbeddedValidator($model);
        }
        return $this->_validator;
    }

    /**
     * @inheritdoc
     */
    private function createEmbeddedValidator($model)
    {
        $rule = $this->rule;
        if ($rule instanceof Validator) {
            return $rule;
        } elseif (is_array($rule) && isset($rule[0])) { // validator type
            if (!is_object($model)) {
                $model = new Model(); // mock up context model
            }
            return Validator::createValidator($rule[0], $model, $this->attributes, array_slice($rule, 1));
        } else {
            throw new InvalidConfigException('Invalid validation rule: a rule must be an array specifying validator type.');
        }
    }

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     * Added attributeFor.
     */
    public function validateAttribute($model, $attribute)
    {
        $attributeFor = empty($this->attributeFor) ? $attribute : $this->attributeFor;

        $value = $model->$attribute;
        if (!is_array($value)) {
            $this->addError($model, $attributeFor, $this->message, []);
            return;
        }

        $validator = $this->getValidator($model); // ensure model context while validator creation

        $originalValue = $model->$attributeFor;
        $originalErrors = $model->getErrors($attributeFor);
        $filteredValue = [];
        foreach ($value as $k => $v) {
            $model->$attributeFor = $v;
            $validator->validateAttribute($model, $attributeFor);
            $filteredValue[$k] = $model->$attributeFor;
            if ($model->hasErrors($attributeFor)) {
                $validationErrors = $model->getErrors($attributeFor);
                $model->clearErrors($attributeFor);
                if (!empty($originalErrors)) {
                    $model->addErrors([$attributeFor => $originalErrors]);
                }
                if ($this->allowMessageFromRule) {
                    $model->addErrors([$attribute => $validationErrors]);
                } else {
                    $this->addError($model, $attribute, $this->message, ['value' => $v]);
                }
                $model->$attributeFor = $originalValue;
                return;
            }
        }
        $model->$attribute = $filteredValue;
    }
}
