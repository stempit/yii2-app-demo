<?php
namespace common\behaviors;

use \yii\base\Behavior;

/**
 * Base class to access to owner's composite types attributes as arrays or other types.
 *
 * To declare an CompositeType class you need to extend [[\common\behaviors\CompositeType]] and
 * implement `getAsArray` and `setAsArray` methods:
 *
 * ```php
 * <?php
 *
 * class JsonBehavior extends \common\behaviors\CompositeType
 * {
 *     public function getAsArray($attribute)
 *     {
 *          $value = $this->owner->{$attribute};
 *          if ($value === null || $value === '{}') {
 *               return [];
 *          }
 *
 *          return json_decode($value, true);
 *     }
 *
 *     public function setAsArray($attribute, $value)
 *     {
 *         $this->owner->$attribute = json_encode($value);
 *     }
 * }
 * ```
 */
abstract class CompositeType extends Behavior
{
    /**
     * @var array Owner's attributes
     */
    public $attributes = [];

    /**
     * @var array or possible types
     */
    protected static $types = ['array'];

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return $this->checkIsAttribute($name) || parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        return $this->checkIsAttribute($name) || parent::canSetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        $attributeAndType = static::getAttributeAndType($name);

        if ($attributeAndType !== null) {
            list ($attribute, $type) = $attributeAndType;
            if (in_array($attribute, $this->attributes, true)) {
                $method = 'getAs' . ucwords($type);

                return $this->{$method}($attribute);
            }
        }

        return parent::__get($name);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        $attributeAndType = static::getAttributeAndType($name);

        if ($attributeAndType !== null) {
            list ($attribute, $type) = $attributeAndType;
            if (in_array($attribute, $this->attributes, true)) {
                $method = 'setAs' . ucwords($type);
                $this->{$method}($attribute, $value);

                return;
            }
        }

        parent::__set($name, $value);
    }

    /**
     * @param string $attribute
     * @return array
     */
    abstract public function getAsArray($attribute);

    /**
     * @param string $attribute
     * @param array $value
     */
    abstract public function setAsArray($attribute, $value);

    /**
     * @param string $name
     * @return bool
     */
    protected function checkIsAttribute($name)
    {
        $attributeToSearch = static::getAttribute($name);

        return $attributeToSearch !== null && in_array($attributeToSearch, $this->attributes, true);
    }

    /**
     * @param string $name
     * @return string|null
     */
    protected static function getAttribute($name)
    {
        if (preg_match('/(.+)_(' . implode('|', static::$types) . ')$/', $name, $match)) {
            return $match[1];
        }

        return null;
    }

    /**
     * @param string $name
     * @return array|null
     */
    protected static function getAttributeAndType($name)
    {
        if (preg_match('/(.+)_(' . implode('|', static::$types) . ')$/', $name, $match)) {
            return [$match[1], $match[2]];
        }

        return null;
    }
}
