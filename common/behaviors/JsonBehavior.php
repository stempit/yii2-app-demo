<?php
namespace common\behaviors;

/**
 * JsonBehavior allows to access to owner's json attributes as array.
 */
class JsonBehavior extends CompositeType
{
    /**
     * @param string $attribute
     * @return array
     */
    public function getAsArray($attribute)
    {
        $value = $this->owner->{$attribute};
        if ($value === null || $value === '{}') {
            return [];
        }

        return json_decode($value, true);
    }

    /**
     * @param string $attribute
     * @param array $value
     */
    public function setAsArray($attribute, $value)
    {
        $this->owner->$attribute = json_encode($value);
    }
}
