<?php
namespace common\behaviors;

/**
 * ArrayBehavior allows to access to owner's array attributes as array or text.
 */
class ArrayBehavior extends CompositeType
{
    /**
     * @var string text delimiter
     */
    public $delimiter = "\n";

    /**
     * @inheritdoc
     */
    protected static $types = ['array', 'text'];

    /**
     * @param string $attribute
     * @return array
     */
    public function getAsArray($attribute)
    {
        $value = $this->owner->{$attribute};
        if ($value === null || $value === '{}') {
            return [];
        }

        $value = str_getcsv(substr($value, 1, -1));

        array_walk($value, function (&$item, $idx) {
            $item = str_replace('\\"', '"', $item);
        });

        return $value;
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function getAsText($attribute)
    {
        return implode($this->delimiter, $this->getAsArray($attribute));
    }

    /**
     * @param string $attribute
     * @param array $value
     */
    public function setAsArray($attribute, $value)
    {
        array_walk($value, function (&$item, $idx) {
            $item = str_replace('"', '\\"', trim($item));
            if ($item !== '') {
                $item = '"' . $item . '"';
            }
        });
        $value = array_filter($value);

        $this->owner->$attribute = '{' . implode(',', $value) . '}';
    }

    /**
     * @param string $attribute
     * @param string $value
     */
    public function setAsText($attribute, $value)
    {
        $this->setAsArray($attribute, explode($this->delimiter, str_replace("\r", '', $value)));
    }
}
