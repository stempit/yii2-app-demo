<?php

namespace common\log;

use Yii;
use \yii\caching\Cache;
use \common\helpers\Inflector;

class EmailTarget extends \yii\log\EmailTarget
{
    /**
     * @var boolean send each category once per day
     */
    public $daily = true;

    /**
     * @var Cache|string the cache object or the ID of the cache application component that
     * is used to cache the table metadata.
     */
    public $schemaCache = 'cache';

    /**
     * @var integer seconds passed after last success to send message again.
     */
    public $skipTimeoutFromSuccess = 1800; // 60 * 30

    /**
     * @inheritdoc
     */
    public function export()
    {
        /* @var $cache Cache */
        $cache = is_string($this->schemaCache) ? Yii::$app->get($this->schemaCache, false) : $this->schemaCache;
        if ($cache instanceof Cache) {
            $time = time();

            foreach ($this->messages as $i => $message) {
                $category = Inflector::camelize($message[2]);

                $lastSuccess = $cache->get('lastSuccess' . $category);
                if (!empty($lastSuccess) && $message[3] - $lastSuccess < $this->skipTimeoutFromSuccess) {
                    unset($this->messages[$i]);
                    continue;
                }

                if ($this->daily) {
                    $cacheKey = 'lastEmail' . $category;
                    $lastSent = $cache->get($cacheKey);
                    if (!empty($lastSent) && $time - $lastSent < 86400) { // 60 * 60 * 24
                        unset($this->messages[$i]);
                        continue;
                    }

                    $cache->set($cacheKey, $time);
                }
            }

            if (empty($this->messages)) {
                return;
            }
        }

        parent::export();
    }
}
