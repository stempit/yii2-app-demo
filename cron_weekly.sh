#!/bin/sh
PROJECT=blabla

PROJECT_PATH=/var/www/${PROJECT}/
export PATH=${PATH}:${PROJECT_PATH}
cd ${PROJECT_PATH}

LOG_PATH=/var/log/cron-out/
mkdir -p ${LOG_PATH}

LOG_PATH=${LOG_PATH}${PROJECT}
./session_clear.sh >>${LOG_PATH}_session-clear.txt 2>&1
./yii db/optimize  >>${LOG_PATH}_db-optimize.txt   2>&1
