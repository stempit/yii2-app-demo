BlaBla Application
==================

Installation
------------

- `composer selfupdate`
- `composer global require "fxp/composer-asset-plugin:^1.2.0"`
- `composer update`
- `php requirements.php`
- `./init`
- session directories
    - `./console/migrations/mod_files ./common/runtime/session 2 5`
    - `chown -R php:www-data ./common/runtime/session`
    - `chmod -R 700 ./common/runtime/session`
- if SELinux enable
    - `restorecon -R .`
    - `chcon -R -t httpd_sys_content_rw_t ./common/runtime`
    - `chcon -R -t httpd_sys_content_rw_t ./console/runtime`
    - `chcon -R -t httpd_sys_content_rw_t ./api/runtime`
    - `chcon -R -t httpd_sys_content_rw_t ./backend/runtime`
    - `chcon -R -t httpd_sys_content_rw_t ./frontend/runtime`
    - `chcon -R -t httpd_sys_content_rw_t ./backend/web/assets`
    - `chcon -R -t httpd_sys_content_rw_t ./frontend/web/assets`
- `./common/config/main-local.php`
- `./yii db/execute-sql --db=false "SELECT 'md5' || md5('password' || 'blabla')"`
- `./yii db/execute-sql --db=false "CREATE ROLE blabla ENCRYPTED PASSWORD '' LOGIN INHERIT"`
- `./yii db/execute-sql --db=false "CREATE DATABASE blabla OWNER blabla ENCODING 'UTF-8'"`
- cron weekly
    - `mv ./cron_weekly.sh /etc/cron.weekly/blabla.sh`
    - `chmod 700 /etc/cron.weekly/blabla.sh`

For Windows, replace all slashes on backslashes.

Console commands
----------------

- `yum update`           update binary packages
- `vendor_update.sh`     update web libraries
- `deploy.sh`            update app code
- `static_compressed.sh` compression assets and gzip pre-compression
- `yii`                  console application methods
- `session_clear.sh`     clear old sessions
