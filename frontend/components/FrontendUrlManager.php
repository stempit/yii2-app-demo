<?php

namespace frontend\components;

use yii\web\UrlManager;

/**
 * FrontendUrlManager is UrlManager for Frontend.
 */
class FrontendUrlManager extends UrlManager
{
    /**
     * @inheritdoc
     */
    public $enablePrettyUrl = true;

    /**
     * @inheritdoc
     */
    public $enableStrictParsing = true;

    /**
     * @inheritdoc
     */
    public $showScriptName = false;

    /**
     * @inheritdoc
     */
    public $cache = 'cacheCommon';

    /**
     * @inheritdoc
     */
    public $rules = [
        '' => 'site/index',
        ['pattern' => 'auth/<action:(login|logout|signup|profile|request\-password\-new|confirm)>', 'route' => 'auth/<action>'],
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setBaseUrl('');
        $this->setScriptUrl('/index.php');

        $appParams = \Yii::$app->params;
        $this->setHostInfo($appParams['frontendScheme'] . '://' . $appParams['frontendDomain']);

        parent::init();
    }
}
