<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use common\helpers\Url;
use common\helpers\Html;
use frontend\assets\AppAsset;
use frontend\widgets\Spaceless;

AppAsset::register($this);
$app = Yii::$app;
$appParams = $app->params;

Spaceless::begin();
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= $app->language; ?>" prefix="og: http://ogp.me/ns#">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta name="theme-color" content="#000000"/>
    <link rel="icon" type="image/png" sizes="192x192" href="<?= Url::to('@web/icon-192x192.png', true); ?>"/>
    <link rel="icon" type="image/x-icon" href="<?= Url::to('@web/favicon.ico', true); ?>" />
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta charset="<?= $app->charset; ?>">
    <title><?= Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
    <?= Html::csrfMetaTags(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<div class="non-footer">
    <header class="page-header">
    </header>

    <div class="page-column">
        <?= $content; ?>
    </div>
</div>

<footer class="page-footer">
    <div class="page-column">
        <small class="copyright">© <?= $appParams['brandName']; ?>, 2014–<?= date('Y'); ?></small>
        <a class="email" href="mailto:<?= $appParams['emailInfo']; ?>"><?= $appParams['emailInfo']; ?></a>
    </div>
</footer>

<svg version="1.1" class="svg-container" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">
</svg>

<?php
if (array_key_exists('yaCounterId', $appParams)) {
    $id = $appParams['yaCounterId'];

    $this->registerJs(<<<JS
        window.yaCounterName = 'yaCounter{$id}';
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter{$id} = new Ya.Metrika({
                        id:{$id},
                        accurateTrackBounce:true,
                        params:window.yamParams||{}
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
JS
        ,
        $this::POS_END,
        'yandex-metrika'
    );
}
if (array_key_exists('googleAnalyticsId', $appParams)) {
    $id = $appParams['googleAnalyticsId'];

    $this->registerJs(<<<JS
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{$id}', 'auto');
        ga('send', 'pageview');
JS
        ,
        $this::POS_END,
        'google-analytics'
    );
}
?>

<?php $this->endBody(); ?>
</body>
</html>
<?php
$this->endPage();
Spaceless::end();
?>
