<?php
use yii\widgets\ActiveForm;
use common\helpers\Html;
use frontend\widgets\Alert;

/**
 * @var $this yii\web\View
 * @var $model \common\models\User
 */

$this->title = Yii::t('common', 'Profile');
?>
<div class="auth-profile">
    <?= Alert::widget(); ?>

    <h4 class="h1"><?= Html::encode($this->title); ?></h4>

    <?php $form = ActiveForm::begin(['id' => 'auth-profile-form']); ?>
        <?= $form->field($model, 'title'      )->textInput(    ['maxlength' => true]); ?>
        <?= $form->field($model, 'passwordNew')->passwordInput(['maxlength' => true])->hint(Yii::t('common', 'Leave it blank if you do not want to change.')); ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Update'), ['class' => 'btn btn-default']); ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
