<?php
use yii\widgets\ActiveForm;
use common\helpers\Url;
use common\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model \frontend\models\SignupForm
 */

$this->title = Yii::t('common', 'Sign up');
?>
<div class="auth-signup">
    <h4 class="h1"><?= Html::encode($this->title); ?></h4>

    <?php $form = ActiveForm::begin(['id' => 'auth-signup-form']); ?>
        <?= $form->field($model, 'email'   )->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'username')->textInput(['maxlength' => true]); ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Sign up'), ['class' => 'btn btn-success']); ?>
        </div>
    <?php ActiveForm::end(); ?>

    <p class="hint-block">
        <?= Yii::t('common', 'Already have an account — {login}.', ['login' => Html::a(Yii::t('common', 'login'), Url::toRoute('login'))]); ?>
        <br/>
        <?= Yii::t('common', 'If you forgot your password — {request} the new one.', ['request' => Html::a(Yii::t('common', 'request'), Url::toRoute('request-password-new'))]); ?>
    </p>
</div>
