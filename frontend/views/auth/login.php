<?php
use yii\widgets\ActiveForm;
use common\helpers\Url;
use common\helpers\Html;
use frontend\widgets\Alert;

/**
 * @var $this yii\web\View
 * @var $model \common\models\LoginForm
 */

$this->title = Yii::t('common', 'Login to site');
?>
<div class="auth-login">
    <?= Alert::widget(); ?>

    <h4 class="h1"><?= Html::encode($this->title); ?></h4>

    <?php $form = ActiveForm::begin(['id' => 'auth-login-form']); ?>
        <?= $form->field($model, 'email'   )->textInput(    ['maxlength' => true]); ?>
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]); ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Login'), ['class' => 'btn btn-default']); ?>
        </div>
    <?php ActiveForm::end(); ?>

    <p class="hint-block">
        <?= Yii::t('common', 'If you forgot your password — {request} the new one.', ['request' => Html::a(Yii::t('common', 'request'), Url::toRoute('request-password-new'))]); ?>
        <br/>
        <?= Yii::t('common', 'Have no account — {sign up}.', ['sign up' => Html::a(Yii::t('common', 'sign up'), Url::toRoute('signup'))]); ?>
    </p>
</div>
