<?php
use yii\widgets\ActiveForm;
use common\helpers\Html;
use common\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $model \common\models\PasswordNewRequestForm
 */

$this->title = Yii::t('common', 'Request new password');
?>
<div class="auth-request-password-new">
    <h4 class="h1"><?= Html::encode($this->title); ?></h4>

    <?php $form = ActiveForm::begin(['id' => 'auth-request-password-new-form']); ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]); ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Request'), ['class' => 'btn btn-info']); ?>
        </div>
    <?php ActiveForm::end(); ?>

    <p class="hint-block">
        <?= Yii::t('common', 'Have no account — {sign up}.', ['sign up' => Html::a(Yii::t('common', 'sign up'), Url::toRoute('signup'))]); ?>
        <br/>
        <?= Yii::t('common', 'If you remember your password — {login}.', ['login' => Html::a(Yii::t('common', 'login'), Url::toRoute('login'))]); ?>
    </p>
</div>
