<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\User;
use frontend\models\SignupForm;

class AuthController extends \common\controllers\AuthController
{
    /**
     * @var string|array
     */
    protected static $urlAfterConfirm = ['auth/profile'];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'only' => ['profile'],
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     */
    public function actionProfile()
    {
        /* @var User $model */
        $model = User::findOne(Yii::$app->getUser()->id);
        $model->setScenario('profile');
        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Profile information updated.'));

            return $this->refresh();
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->signup()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Further instructions sent to your email.'));

                return $this->redirect(['auth/login']);
            } else {
                //TODO: this message should be sent to the render (by validation error?), it's not necessary to do this through the session
                Yii::$app->getSession()->setFlash('error', Yii::t('common', 'Email send failed. Please try again later.'));
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
