<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    /**
     * @var string|null
     */
    public $email;

    /**
     * @var string|null
     */
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'username'], 'filter', 'filter' => 'trim'],
            ['email', 'filter', 'filter' => 'mb_strtolower'],

            [['email', 'username'], 'required'],
            ['email',    'string', 'min' => 6, 'max' => 255],
            ['username', 'string', 'min' => 2, 'max' => 95],
            ['email', 'email', 'enableIDN' => true],

            [
                'email',
                'unique',
                'targetClass' => '\common\models\User',
                'filter' => function($query) {
                    /* @var \common\models\UserQuery $query */
                    $query->emailVerified();
                },
                'message' => \Yii::t('common', 'User with this email already exists, request new password.'),
            ],
            [
                'username',
                'unique',
                'targetClass' => '\common\models\User',
                'targetAttribute' => 'title',
                'filter' => function($query) {
                    /* @var \common\models\UserQuery $query */
                    $query->emailVerified();
                },
                'message' => \Yii::t('common', 'User with this title already exists, choose another.'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('common', 'Username'),
        ];
    }

    /**
     * @return \common\models\User|null the saved model or null if saving fails
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        /* @var \common\models\User $user */
        $user = User::findOne(['email' => $this->email]);
        if ($user === null) {
            $user = new User();
            $user->email = $this->email;
            $user->role = 'registered';
        } elseif ($user->role !== 'registered') {
            $user->role = 'registered';
        }
        $user->title = $this->username;
        $user->setPassword(\Yii::$app->getSecurity()->generateRandomString());

        $user->setScenario('signup');
        if (!$user->save()) {
            return null;
        }
        return $user;
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        $user = User::find()
            ->tokenByEmail($this->email)
            ->one();

        return \Yii::$app->getMailer()
            ->compose(
                [
                    'html' => 'authSignup-html',
                    'text' => 'authSignup-text',
                ],
                [
                    'username' => $this->username,
                    'token'    => $user->token,
                ]
            )
            ->setTo($this->email)
            ->setSubject(\Yii::t('common', 'Signing up'))
            ->send();
    }
}
