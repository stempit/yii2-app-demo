<?php
namespace frontend\assets;

class AppAsset extends AppAssetBundle
{
    /**
     * @inheritdoc
     */
    public $css = [
        'css/first-screen.css',
        'css/non-first-screen.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'js/main.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
