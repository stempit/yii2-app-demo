<?php
/**
 * Configuration file for the "yii asset" console command.
 * Note that in the console environment, some path aliases like '@webroot' and '@web' may not exist.
 * Please define these missing path aliases.
 */
$basePath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'web';
return [
     'jsCompressor' => 'yii compress/js  {from} {to}',
    'cssCompressor' => 'yii compress/css {from} {to}',
    'bundles' => [
        'frontend\assets\AppAsset',
    ],
    'targets' => [
        'frontend\assets\AppCompressedAsset' => [
            'basePath' => $basePath,
            'baseUrl' => '',
            'js'  => 'assets/app-{hash}.js',
            'css' => 'assets/app-{hash}.css',
            'depends' => [
                'yii\web\JqueryAsset',
                'yii\web\YiiAsset',
                'frontend\assets\AppAsset',
            ],
        ],
    ],
    'assetManager' => [
        'basePath' => $basePath . DIRECTORY_SEPARATOR . 'assets',
        'baseUrl' => '/assets',
        'linkAssets' => strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN',
    ],
];
