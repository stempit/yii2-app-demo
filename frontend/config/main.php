<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$isWin = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';

$assetManagerCompressedFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'assets_compressed.php';

return [
    'id' => 'blabla-frontend',
    'name' => $params['brandName'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'session' => [
            'class' => 'common\components\Session',
            'name' => 'BLABLA',
            'useTransparentSessionID' => false,
            'useCookies' => true,
            'cookieParams' => [
                'lifetime' => 31536000, // 60*60*24*365
                'path' => '/',
                'domain' => substr($params['backendDomain'], strpos($params['backendDomain'], '.')),
                'secure' => false,
            ],
            'gCProbability' => 0,
            'savePath' => '2;@common/runtime/session',
            'hashBitsPerCharacter' => 5,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'authTimeout' => 237600, // 60 * 60 * ((24 - 18) + 48 + 9 + 3)
            'loginUrl' => ['auth/login'],
        ],
        'log' => [
            'targets' => [
                'notFound' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/404.log',
                    'rotateByCopy' => $isWin,
                    'levels' => ['error'],
                    'categories' => ['yii\web\HttpException:404'],
                    'logVars' => [],
                ],
                'errorCommon' => [
                    'except' => ['yii\web\HttpException:404'],
                ],
            ],
        ],
        'mailer' => [
            'messageConfig' => [
                'from' => [$params['emailInfo'] => $params['brandName']],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'frontend\components\FrontendUrlManager',
        ],
        'assetManager' => [
            'linkAssets' => !$isWin,
            'bundles' => is_file($assetManagerCompressedFile) ? require($assetManagerCompressedFile) : [],
        ],
    ],
    'params' => $params,
];
