<?php
return [
    'defaultOgImage' => '/icon310.png',

    'mainTitle'           => 'Главная страница',
    'mainOgTitle'         => 'Главная страница',
    'mainMetaDescription' => 'Описание.',
    'mainOgDescription'   => 'Описание.',
    'mainKeywords'        => 'ключевые слова',

    //'yaCounterId' => '',
    //'googleAnalyticsId' => '',
];
