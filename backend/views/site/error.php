<?php

use common\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * @var $this yii\web\View
 * @var $name string
 * @var $message string
 * @var $exception Exception
 */

$this->title = $name;
?>
<div class="error-wrapper">

    <h1><?= Html::encode($this->title); ?></h1>

    <p>
        <?= nl2br(Html::encode($message)); ?>
    </p>

    <p>
    <?php
    if ($exception instanceof NotFoundHttpException) {
        ?><?= Yii::t('common', 'You can go to the homepage or find page yourself.'); ?><?php
    } elseif ($exception instanceof ForbiddenHttpException) {
        ?><?= Yii::t('common', 'Login under an account with necessary privileges.'); ?><?php
    } else {
        ?><?= Yii::t('common', 'We are already fixing the error.'); ?><?php
    }
    ?>
    </p>

</div>
