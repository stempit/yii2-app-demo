<?php

use yii\helpers\Inflector;

/**
 * @var yii\web\View $this
 * @var yii\db\ActiveRecord $model
 */

$this->title = Yii::t('common', 'Creating') . ' \ ' . constant($model::className() . '::LABEL_PLURAL_' . str_replace('-', '_', Yii::$app->language));
?>

<h1><?= Yii::t('common', 'Creating'); ?></h1>

<?= $this->render('/' . Inflector::underscore($model->formName()) . '/_form', [
    'model' => $model,
]); ?>
