<?php

use yii\helpers\Inflector;

/**
 * @var yii\web\View $this
 * @var yii\db\ActiveRecord $model
 */

$modelLabel = constant($model::className() . '::LABEL_SINGULAR_' . str_replace('-', '_', Yii::$app->language));

$this->title = $model->id . ' \ ' . $modelLabel
?>

<h1><?= $modelLabel; ?> <?= $model->id; ?></h1>

<?php echo $this->render('/' . Inflector::underscore($model->formName()) . '/_view', [
    'model' => $model,
]); ?>
