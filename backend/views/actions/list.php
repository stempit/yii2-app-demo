<?php

use common\helpers\StringHelper;
use common\helpers\Url;
use common\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var string $modelLabel
 * @var boolean $withCreate
 * @var array $filters
 * @var string|boolean $filter_active
 * @var common\models\AbstractSearch $filterModel
 * @var array $configGridView
 * @var boolean $withAutoUpdate
 */
?>
<div class="btn-toolbar pull-right">
<?php
if ($withCreate) {
    echo Html::a(Yii::t('common', 'Create'), ['create'], ['class' => 'btn btn-success pull-right']);
}

if ($filters !== []) {
    ?><div class="btn-group pull-right"><?php
    foreach ($filters as $name => $value) {
        $class = 'btn btn-info';
        if ($name === $filter_active) {
            $class .= ' active';
        }

        echo Html::a($value, Url::current(['filter' => $name]), ['class' => $class]);
    }
    ?></div><?php
}
if ($filterModel->isAttributeActive('any_field')) {
    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'pull-right form-inline'],
    ]);
    echo $form->field($filterModel, 'any_field', [
        'options' => ['class' => 'input-group'],
        'template' => '{input}{button}',
        'parts' => ['{button}' => '<span class="input-group-btn">' . Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) . '</span>'],
    ]);
    ActiveForm::end();
}
?>
</div>

<h1><?= Yii::t('common', 'List'); ?> <?php
    if ($filter_active) {
        $this->title = $filters[$filter_active];
        echo StringHelper::mb_lcfirst($this->title);
    } else {
        $this->title = Yii::t('common', 'All');
        echo Yii::t('common', 'of all');
    }
    $this->title .= ' \ ' . $modelLabel;
?></h1><?php

if ($withAutoUpdate) {
    $configGridView['id'] = 'grid-container';
    $checkboxHtmlId = 'grid-auto_update';

    echo Html::checkbox($checkboxHtmlId, false, [
        'id' => $checkboxHtmlId,
        'label' => Yii::t('common', 'Refresh automatically'),
    ]);

    $this->registerJs(<<<JS
$('#{$checkboxHtmlId}').on('change', function(e) {
     window.autoUpdateGrid();
});

$('#pjax-container').on('afterFilter', '#{$configGridView['id']}', function(){
    if ($('#{$checkboxHtmlId}').prop('checked')) {
        window.autoUpdateGridTimeout = setTimeout(window.autoUpdateGrid, 7000);
    }
});

window.autoUpdateGrid = function() {
    if ($('#{$checkboxHtmlId}').prop('checked')) {
        jQuery.fn.yiiGridView.apply(document.getElementById('{$configGridView['id']}'), ['applyFilter']);
    } else if (typeof window.autoUpdateGridTimeout !== 'undefined') {
        clearTimeout(window.autoUpdateGridTimeout);
    }
}
JS
    , $this::POS_READY, $checkboxHtmlId);
}

Pjax::begin([
    'id' => 'pjax-container',
]);
echo GridView::widget($configGridView);
Pjax::end();
