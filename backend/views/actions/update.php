<?php

use yii\helpers\Inflector;
use common\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\db\ActiveRecord $model
 */

$this->title = $model->id . ' \ ' . constant($model::className() . '::LABEL_PLURAL_' . str_replace('-', '_', Yii::$app->language));
?>

<p class="btn-group pull-right">
    <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger', 'data-confirm' => Yii::t('common', 'Are you sure you want to delete?')]); ?>
</p>

<h1><?= Yii::t('common', 'Updating'); ?> <?= $model->id; ?></h1>

<?= $this->render('/' . Inflector::underscore($model->formName()) . '/_form', [
    'model' => $model,
]); ?>
