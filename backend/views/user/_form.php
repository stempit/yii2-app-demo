<?php

use yii\bootstrap\ActiveForm;
use common\helpers\Html;
use common\models\User;

/**
 * @var $this yii\web\View
 * @var $model common\models\User
 */
?>
<div class="row">
<?php $form = ActiveForm::begin(['id' => 'user-form']); ?>
    <div class="col-lg-8">
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'role')->dropDownList(User::dictionaryRoleLabels()); ?>
        <?= $form->field($model, 'email'); ?>
        <?= $form->field($model, 'first_name'); ?>
        <?= $form->field($model, 'last_name'); ?>
        <?= $form->field($model, 'timezone'); ?>

        <?= $form->errorSummary($model); ?>
        <div class="text-center">
            <?= Html::submitButton(
                $model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']
            ); ?>
        </div>
    </div>
<?php
if (!$model->isNewRecord) {
    $formatter = Yii::$app->formatter;
?>
    <div class="col-lg-4">
        <div class="panel panel-info">
            <div class="panel-heading"><?= Yii::t('common', 'Information'); ?></div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt><?= $model->getAttributeLabel('created'); ?></dt>
                    <dd><?= $formatter->asDatetime($model->created); ?></dd>
                    <dt><?= $model->getAttributeLabel('updated'); ?></dt>
                    <dd><?= $formatter->asDatetime($model->updated); ?></dd>
                </dl>
            </div>
        </div>
    </div>
<?php
}

ActiveForm::end();
?>
</div>
