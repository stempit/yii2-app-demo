<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use backend\assets\AppAsset;
use yii\bootstrap\NavBar;
use common\models\User;
use common\helpers\Url;
use common\helpers\Html;
use backend\widgets\NavActiveByController;
use backend\widgets\Alert;

AppAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="theme-color" content="#000000"/>
    <link rel="icon" type="image/png" sizes="192x192" href="<?= Url::to('@web/icon-192x192.png', true); ?>"/>
    <link rel="icon" type="image/x-icon" href="<?= Url::to('@web/favicon.ico', true); ?>" />
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta charset="<?= Yii::$app->charset; ?>"/>
    <title><?= Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
    <?= Html::csrfMetaTags(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<div class="non-footer">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('common', 'To site'),
        'brandUrl' => Yii::$app->frontendUrlManager->getHostInfo(),
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $user = Yii::$app->getUser();
    $isGuest = $user->getIsGuest();

    $menuItems = [];
    if ($isGuest) {
        $menuItems[] = ['label' => Yii::t('common', 'Login'), 'url' => $user->loginUrl];
    } else {
        $menuItems[] = [
            'label' => $user->getIdentity()->title,
            'items' => [
                ['label' => Yii::t('common', 'Logout'), 'url' => ['auth/logout']],
            ],
        ];
    }
    echo NavActiveByController::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);

    if (!$isGuest && in_array($user->getIdentity()->role, User::$rolesBackend, true)) {
        echo NavActiveByController::widget([
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items' => [
                ['label' => Yii::t('common', 'Dashboard'), 'url' => ['dashboard/index']],
                ['label' => Yii::t('common', 'Users'    ), 'url' => ['user/index']],
            ],
        ]);
    }

    NavBar::end();
    ?>

    <div class="container">
        <?= Alert::widget(); ?>
        <?= $content; ?>
    </div>
</div>

<footer class="navbar navbar-default">
    <div class="container">
        <p class="navbar-text pull-left">© <?= Yii::$app->params['brandName']; ?>, 2014–<?= date('Y'); ?></p>

        <p class="navbar-text pull-right"><?= Html::mailto(Yii::$app->params['emailSupport']); ?></p>
    </div>
</footer>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
