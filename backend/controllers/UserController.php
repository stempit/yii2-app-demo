<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\grid\ActionColumn;
use common\controllers\AbstractController;
use common\models\User;
use backend\actions\CreateAction;
use backend\actions\DeleteAction;
use backend\actions\ListAction;
use backend\actions\UpdateAction;
use backend\widgets\RangeColumn;

class UserController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return in_array(\Yii::$app->getUser()->getIdentity()->role, \common\models\User::$rolesBackend, true);
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function actions()
    {
        return [
            'update' => UpdateAction::className(),
            'delete' => DeleteAction::className(),
            'index' => [
                'class' => ListAction::className(),
                'configGridView' => [
                    'columns' => [
                        [
                            'attribute' => 'role',
                            'filter' => User::dictionaryRoleLabels(),
                            'value' => function ($model) {
                                /** @var User $model */
                                return User::dictionaryRoleLabels()[$model->role];
                            }
                        ],
                        'email',
                        'title',
                        [
                            'class' => RangeColumn::className(),
                            'attribute' => 'created',
                            'format' => 'datetime',
                            'filterInputType' => 'datetime-local',
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update}',
                        ],
                    ],
                ],
            ],
            'create' => CreateAction::className(),
        ];
    }
}
