<?php

namespace backend\models;

use common\models\AbstractSearch;
use common\models\User;
use common\validators\DatePgValidator;
use yii\data\ActiveDataProvider;

class UserSearch extends AbstractSearch
{
    /**
     * @var string|null
     */
    public $role;

    /**
     * @var string|null
     */
    public $email;

    /**
     * @var string|null
     */
    public $title;

    /**
     * @var string|null
     */
    public $created_min;

    /**
     * @var string|null
     */
    public $created_max;

    /**
     * @var string|null
     */
    public $any_field;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'email', 'title', 'any_field'], 'filter', 'filter' => 'trim'],
            [['role', 'email', 'title', 'any_field'], 'string'],
            [['created_min', 'created_max'], DatePgValidator::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass()
    {
        return 'common\\models\\' . parent::getBaseModelClass();
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\UnknownPropertyException
     */
    public function search($params)
    {
        /** @var User $baseModelClass */
        $baseModelClass = $this->getBaseModelClass();

        $query = $baseModelClass::find();
        if (!empty($params['filter'])) {
            $query->{$params['filter']}();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);

        $dataProvider->getSort()->attributes['created']['default'] = SORT_DESC;

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $this->addConditionEqual($query, 'role');
        foreach (['email', 'title'] as $attribute) {
            $this->addConditionLike($query, $attribute);
        }
        $this->addConditionLess($query, 'created', 'created_max');
        $this->addConditionMore($query, 'created', 'created_min');
        $this->addConditionLikeMultiple($query, [
            'email',
            'title',
            'first_name',
            'last_name',
        ], 'any_field');

        return $dataProvider;
    }
}
