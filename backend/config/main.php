<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$isWin = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
return [
    'id' => 'blabla-backend',
    'name' => 'admin section of ' . $params['brandName'],
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute' => 'dashboard',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'session' => [
            'class' => 'common\components\Session',
            'name' => 'BLABLA',
            'useTransparentSessionID' => false,
            'useCookies' => true,
            'cookieParams' => [
                'lifetime' => 31536000, // 60*60*24*365
                'path' => '/',
                'domain' => substr($params['backendDomain'], strpos($params['backendDomain'], '.')),
                'secure' => false,
            ],
            'gCProbability' => 0,
            'savePath' => '2;@common/runtime/session',
            'hashBitsPerCharacter' => 5,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'authTimeout' => 237600, // 60 * 60 * ((24 - 18) + 48 + 9 + 3)
            'loginUrl' => ['auth/login'],
        ],
        'log' => [
            'targets' => [
                'access' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/403.log',
                    'rotateByCopy' => $isWin,
                    'levels' => ['error'],
                    'categories' => ['yii\web\HttpException:403'],
                ],
                'notFound' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/404.log',
                    'rotateByCopy' => $isWin,
                    'levels' => ['error'],
                    'categories' => ['yii\web\HttpException:404'],
                    'logVars' => [],
                ],
                'sql' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/sql.log',
                    'rotateByCopy' => $isWin,
                    'levels' => ['info'],
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                ],
                'errorCommon' => [
                    'except' => [
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                ],
                'emailError' => [
                    'class' => 'common\log\EmailTarget',
                    'schemaCache' => 'cache',
                    'levels' => ['error'],
                    'logVars' => [],
                    'except' => [
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'message' => [
                        'to' => [$params['emailBugs']],
                        'subject' => 'Backend error',
                    ],
                ],
            ],
        ],
        'mailer' => [
            'messageConfig' => [
                'from' => [$params['emailInfo'] => $params['brandName']],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['pattern' => 'auth/<action:(login|logout|request\-password\-new|confirm)>', 'route' => 'auth/<action>'],
            ],
        ],
        'frontendUrlManager' => [
            'class' => 'frontend\components\FrontendUrlManager',
        ],
        'formatter' => [
            'datetimeFormat' => 'long',
        ],
    ],
    'params' => $params,
];
