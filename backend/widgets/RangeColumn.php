<?php
namespace backend\widgets;

use yii\grid\DataColumn;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\web\View;
use yii\helpers\ArrayHelper;
use common\helpers\Html;

/**
 * DataColumn is the column type for the [[GridView]] widget.
 *
 * It is used to display data columns and sorting ranges.
 *
 * @author Kostya M <iamF13@ya.ru>
 */
class RangeColumn extends DataColumn
{
    /**
     * @var string
     */
    public $attribute_min;

    /**
     * @var string
     */
    public $attribute_max;

    /**
     * @var string
     */
    public $attribute_presets;

    /**
     * @var array
     */
    public $presets = [];

    /**
     * @var string
     */
    public $rangeDelimiter = ' – ';

    /**
     * @var string
     */
    public $filterInputType = 'text';

    /**
     * @return string
     * @throws InvalidConfigException
     */
    protected function renderFilterCellContent()
    {
        if ($this->filter === false) {
            throw new InvalidConfigException('The "filter" property must be true or default.');
        }
        if (!($this->grid->filterModel instanceof Model)) {
            throw new InvalidConfigException('The "filterModel" property must be Model.');
        }
        if ($this->attribute === null) {
            throw new InvalidConfigException('The "attribute" property must be set.');
        }
        if ($this->attribute_min === false && $this->attribute_max === false) {
            throw new InvalidConfigException('The "attribute_min" or "attribute_max" property must be not false.');
        }

        if ($this->attribute_min === null) {
            $this->attribute_min = $this->attribute . '_min';
        }
        if ($this->attribute_max === null) {
            $this->attribute_max = $this->attribute . '_max';
        }
        if ($this->attribute_presets === null) {
            $this->attribute_presets = $this->attribute . '_filter';
        }

        if (!($this->attribute_min === false || $this->grid->filterModel->isAttributeActive($this->attribute_min))) {
            throw new InvalidConfigException('The "attribute_min" property must be active.');
        }
        if (!($this->attribute_max === false || $this->grid->filterModel->isAttributeActive($this->attribute_max))) {
            throw new InvalidConfigException('The "attribute_max" property must be active.');
        }

        if ($this->attribute_min === false) {
            $out = '';
        } else {
            $inputOptions = $this->filterInputOptions;
            $inputOptions['class'] .= ' left50p';
            $inputOptions['id'] = $this->grid->id . '__' . $this->attribute_min;

            $out = Html::activeInput($this->filterInputType, $this->grid->filterModel, $this->attribute_min, $inputOptions);
        }
        if ($this->attribute_max !== false) {
            $inputOptions = $this->filterInputOptions;
            $inputOptions['class'] .= ' right50p';
            $inputOptions['id'] = $this->grid->id . '__' . $this->attribute_max;

            $out .= Html::activeInput($this->filterInputType, $this->grid->filterModel, $this->attribute_max, $inputOptions);
        }

        if (!empty($this->presets)) {
            $presetItems = [];

            foreach ($this->presets as $item) {
                $presetItems[$item['min'] . $this->rangeDelimiter . $item['max']] = $item['title'];
            }

            $options = array_merge(['prompt' => ''], $this->filterInputOptions);
            $options['class'] .= ' range-filter';
            $options['id'] = $this->grid->id . '__' . $this->attribute_presets;

            if (!array_key_exists('options', $options)) {
                $options['options'] = [];
            }

            $presetItemWithClass = array_filter(ArrayHelper::map($this->presets, 'title', 'class'), function ($item) {
                return !is_null($item);
            });
            foreach ($presetItems as $value => $title) {
                $itemOptions = [
                    'title' => $value,
                ];

                if (array_key_exists($title, $presetItemWithClass)) {
                    $itemOptions['class'] = $presetItemWithClass[$title];
                }

                $options['options'][$value] = $itemOptions;
            }

            $out .= Html::activeDropDownList($this->grid->filterModel, $this->attribute_presets, $presetItems, $options);
            $this->grid->getView()->registerJs(<<<JS
                $('#{$options['id']}').change(function() {
                    var values = this.value.split('{$this->rangeDelimiter}');
                    if (values.length !== 2) {
                        values = ['', ''];
                    }

                    $('#{$this->grid->id}__{$this->attribute_min}').val(values[0]);
                    $('#{$this->grid->id}__{$this->attribute_max}').val(values[1]);
                });
JS
                ,
                View::POS_READY,
                $options['id']
            );
        }

        return $out;
    }
}
