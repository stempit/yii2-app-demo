<?php
namespace backend\actions;

use Yii;
use yii\base\Action;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * UpdateAction represents an action for update.
 *
 * @property \common\controllers\AbstractController $controller
 */
class DeleteAction extends Action
{
    /**
     * @var string view
     */
    public $view = 'delete';

    /**
     * @var string model class
     */
    public $modelClass;

    /**
     * @var string|array url for return after success update
     */
    public $returnUrl;

    /**
     * @var callable a condition to deny action
     */
    public $denyCondition;

    /**
     * @param string|int|array $id model id
     * @return mixed the result of the action
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws ForbiddenHttpException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\base\InvalidParamException
     */
    public function run($id)
    {
        $modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

        /** @var \yii\db\ActiveRecord $model */
        $model = $modelClass::findOne($id);
        if($model === null) {
            throw new NotFoundHttpException('The requested item does not exist.');
        }

        if (!empty($this->denyCondition) && call_user_func($this->denyCondition, $model)) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        if ($model->delete()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Deleted'));

            if ($this->returnUrl === null) {
                $this->returnUrl = ['index'];
            }
            return $this->controller->redirect($this->returnUrl);
        }

        return $this->controller->render('/actions/' . $this->view, [
            'model' => $model,
        ]);
    }
}
