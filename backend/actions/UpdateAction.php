<?php
namespace backend\actions;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

/**
 * UpdateAction represents an action for update.
 *
 * @property \common\controllers\AbstractController $controller
 */
class UpdateAction extends Action
{
    /**
     * @var string view
     */
    public $view = 'update';

    /**
     * @var string model class
     */
    public $modelClass;

    /**
     * @var string model scenario
     */
    public $modelScenario = 'administrator';

    /**
     * @var string|array url for return after success update
     */
    public $returnUrl;

    /**
     * @param string|int|array $id model id
     * @return mixed the result of the action
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidParamException
     */
    public function run($id)
    {
        $modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

        /** @var \yii\db\ActiveRecord|null $model */
        $model = $modelClass::findOne($id);
        if($model === null) {
            throw new NotFoundHttpException('The requested item does not exist.');
        }

        $model->setScenario($this->modelScenario);
        if ($model->load($_POST)) {
            if ($model->validate()) {
                set_time_limit(180);
                if ($model->save(false)) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Updated'));

                    if ($this->returnUrl) {
                        return $this->controller->redirect($this->returnUrl);
                    } else {
                        return $this->controller->refresh();
                    }
                } else {
                    Yii::$app->getResponse()->setStatusCode(400);
                }
            } else {
                Yii::$app->getResponse()->setStatusCode(422, 'Validation Failed');
            }
        }

        return $this->controller->render('/actions/' . $this->view, [
            'model' => $model,
        ]);
    }
}
