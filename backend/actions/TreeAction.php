<?php
namespace backend\actions;

use yii\base\Action;

/**
 * TreeAction represents an action for list as tree.
 *
 * @property \common\controllers\AbstractController $controller
 */
class TreeAction extends Action
{
    /**
     * @var string view
     */
    public $view = 'tree';

    /**
     * @var string model class
     */
    public $modelClass;

    /**
     * @return mixed the result of the action
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

        return $this->controller->render('/actions/' . $this->view, [
            'modelLabel' => constant($modelClass . '::LABEL_PLURAL_' . str_replace('-', '_', \Yii::$app->language)),
            'items' => $modelClass::find()->forTree()->all(),
            'withCreate' => method_exists($this->controller, 'actionCreate') || array_key_exists('create', $this->controller->actions()),
        ]);
    }
}
