<?php
namespace backend\actions;

use Yii;
use \yii\base\Action;
use \yii\web\NotFoundHttpException;

/**
 * DeleteSoftAction represents an action for soft delete. Requires is_visible attribute in model.
 *
 * @property \common\controllers\AbstractController $controller
 */
class DeleteSoftAction extends Action
{
    /**
     * @var string view
     */
    public $view = 'delete';

    /**
     * @var string model class
     */
    public $modelClass;

    /**
     * @var string attribute to update
     */
    public $attribute = 'is_visible';

    /**
     * @var mixed value to set
     */
    public $value = false;

    /**
     * @var string model scenario
     */
    public $modelScenario = 'administrator';

    /**
     * @var string|array url for return after success update
     */
    public $returnUrl;

    /**
     * @param string|int|array $id model id
     * @return mixed the result of the action
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\UnknownPropertyException
     */
    public function run($id)
    {
        $modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

        /** @var \yii\db\ActiveRecord $model */
        $model = $modelClass::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('The requested item does not exist.');
        }

        $model->setScenario($this->modelScenario);
        $model->{$this->attribute} = $this->value;

        if ($model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('common', $model->is_visible ? 'Restored' : 'Deleted'));

            if ($this->returnUrl === null) {
                $this->returnUrl = ['index'];
            }

            return $this->controller->redirect($this->returnUrl);
        }

        return $this->controller->render('/actions/' . $this->view, [
            'model' => $model,
        ]);
    }
}
