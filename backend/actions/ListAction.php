<?php
namespace backend\actions;

use Yii;
use yii\base\Action;
use common\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * ListAction represents an action for list.
 *
 * @property \common\controllers\AbstractController $controller
 */
class ListAction extends Action
{
    /**
     * @var string view
     */
    public $view = 'list';

    /**
     * @var string filter class
     */
    public $filterClass;

    /**
     * @var string filter scenario
     */
    public $filterScenario = 'search';

    /**
     * @var string|boolean default filter
     */
    public $defaultFilter = false;

    /**
     * @var array config GridView
     */
    public $configGridView = [];

    /**
     * @var boolean auto refresh page
     */
    public $withAutoUpdate = false;

    /**
     * @return mixed the result of the action
     * @throws NotFoundHttpException if wrong filter
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $filterClass = $this->filterClass ? $this->filterClass : 'backend\\models\\' . $this->controller->getBaseModelClass() . 'Search';

        /** @var \common\models\AbstractSearch $filterModel */
        $filterModel = new $filterClass(/*$this->filterScenario*/);

        $filters = method_exists($filterModel, 'searchLabels') ? $filterModel->searchLabels() : [];
        if (empty($_GET['filter'])) {
            if (!empty($this->defaultFilter)) {
                $this->controller->redirect(Url::current(['filter' => $this->defaultFilter]));
            }

            $filter_active = false;
        } else {
            $filter_active = $_GET['filter'];

            if (!array_key_exists($filter_active, $filters)) {
                throw new NotFoundHttpException('The requested filter does not exist.');
            }
        }

        $modelClass = $filterModel->getBaseModelClass();

        $configGridView = $this->configGridView;
        $configGridView['filterModel']  = $filterModel;
        $configGridView['dataProvider'] = $filterModel->search(Yii::$app->getRequest()->getQueryParams());

        $formName = $filterModel->formName();
        $configGridView['filterUrl'] = Yii::$app->getRequest()->getQueryParams();
        $configGridView['filterUrl'][0] = '/' . Yii::$app->controller->getRoute();
        if (!empty($_GET[$formName]['text'])) {
            $configGridView['filterUrl'][$formName]['text'] = $_GET[$formName]['text'];
        }


        return $this->controller->render('/actions/' . $this->view, [
            'modelLabel' => constant($modelClass . '::LABEL_PLURAL_' . str_replace('-', '_', Yii::$app->language)),
            'filterModel' => $filterModel,
            'configGridView' => $configGridView,
            'withCreate' => method_exists($this->controller, 'actionCreate') || array_key_exists('create', $this->controller->actions()),
            'filters' => $filters,
            'filter_active' => $filter_active,
            'withAutoUpdate' => $this->withAutoUpdate,
        ]);
    }
}
