<?php

namespace backend\actions;

use yii\base\Action;
use yii\web\NotFoundHttpException;

/**
 * ViewAction represents an action for view.
 * @property \common\controllers\AbstractController $controller
 */
class ViewAction extends Action
{
    /**
     * @var string view
     */
    public $view = 'view';

    /**
     * @var string model class
     */
    public $modelClass;

    /**
     * @param string|int|array $id model id
     * @return mixed the result of the action
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidParamException
     */
    public function run($id)
    {
        $modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

        /** @var \yii\db\ActiveRecord|null $model */
        $model = $modelClass::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('The requested item does not exist.');
        }

        return $this->controller->render('/actions/' . $this->view, [
            'model' => $model,
        ]);
    }
}
