#!/bin/sh
export LC_NUMERIC="en_US.UTF-8"

function perform_clear()
{
    echo -n $1' cleanup'
    DIRECTORY_SESSION='./'$1'/'

    time_start=$(date "+%m-%d %H:%M:%S")
    echo -n ' started at '${time_start}

    time_start=$(date +%s.%N)

    find ${DIRECTORY_SESSION} -type f -name sess_* -mtime +7 -size 0 -exec rm -rf '{}' ';' 2>/dev/null
    echo -n ', finished'
    find ${DIRECTORY_SESSION} -type f -name sess_* -mtime +30        -exec rm -rf '{}' ';' 2>/dev/null

    time_duration=$(echo "$(date +%s.%N) - $time_start" | bc)
    printf " in %.3f seconds.\n" $time_duration
}

perform_clear 'common/runtime/session'
